---
title: Rich Kids Stay Rich, Poor Kids Stay Poor
author: ""
date: '2016-02-01'
slug: casselman-flowers-rich-kids-stay-rich-poor-kids-stay-poor
categories:
  - Economic Justice
tags:
  - Equality of Opportunity
subtitle: ''
summary: ''
authors: 
- Ben Casselman
- Andrew Flowers
- (FiveThirtyEight)
lastmod: '2016-02-01'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: 
- opportunity-insights
url_source: https://fivethirtyeight.com/features/rich-kids-stay-rich-poor-kids-stay-poor
---

On Friday, a team of researchers led by Stanford economist Raj Chetty released a [paper](http://equality-of-opportunity.org/images/gender_paper.pdf) on how growing up in poverty affects boys and girls differently. Their core finding: Boys who grow up in poor families fare substantially worse in adulthood, in terms of employment and earnings, than girls who grow up in the same circumstances. (The Washington Post has a good [write-up](https://www.washingtonpost.com/news/wonk/wp/2016/01/29/the-unique-power-of-poverty-to-turn-young-boys-into-jobless-men) of the paper and its implications.)

But beyond its immediate conclusions, the paper, like much of Chetty’s recent work as part of his [Equality of Opportunity Project](http://equality-of-opportunity.org/), points to a deeper truth: In the U.S., where you come from — where you grow up, how much your parents earn, whether your parents were married — plays a major role in determining where you will end up later in life.

Take, for example, the chart below, a version of which was Figure 1 in the recent paper. It shows how likely someone is to have a job at age 30 (the y-axis) based on how much money his or her parents made when he or she was in high school (the x-axis). In this paper, Chetty focused on the difference between men and women, which is striking; men from middle-class and affluent families are more likely to work than women, but among the poor, the opposite is true.^[Chetty et al. define “work” as having any wage or salary earnings (as reported on a W-2 form). They find similar trends when they use a broader definition that includes non-wage earnings, such as self-employment income.]

[Continue reading](https://fivethirtyeight.com/features/rich-kids-stay-rich-poor-kids-stay-poor)
