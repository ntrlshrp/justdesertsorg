---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Income-Inequality.Info"
subtitle: "Learn about global income inequality through an interactive visualization"
summary: ""
authors: 
- Boris Yakubchik
- Branko Milanovic
- (Income-Inequality.Info)
tags: 
- Economic Inequality
- International
- Income
categories: []
date: 2019-05-27
lastmod: 2019-05-31
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: 'Image credit: [Income-Inequality.info](https://income-inequality.info/images/5.png)'
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
url_source: https://income-inequality.info/
url_code: https://github.com/whyboris/income-inequality.info
url_code: https://github.com/whyboris/Global-Income-Distribution
---

Inequality is a broad multifaceted concept that is impossible to reduce to a single number or visualization; to understand it better, we must look at numerous perspectives (income, wealth, education, health, access to technology, and a multitude more). Below is just a single exploration: a snapshot in time comparing the distributions of individual incomes of people within countries and across the world.

You can [skip to the interactive visualization](https://income-inequality.info/#viz), but you will get a richer picture of the data by reading the essay first. 

[Continue Reading](https://income-inequality.info/)
