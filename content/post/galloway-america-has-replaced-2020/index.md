---
title: "America Has Replaced Capitalism With Cronyism"
date: 2020-04-01
publishDate: 2020-04-25T23:01:14.258219Z
authors: ["Scott Galloway", "(New York Magazine)"]
publication_types: ["0"]
abstract: "Trump has adopted a narrative that the coronavirus stimulus is about protecting the most vulnerable, but it’s about buttressing the most wealthy."
featured: false
publication: "*Intelligencer*"
url_source: "https://nymag.com/intelligencer/2020/04/scott-galloway-america-replaced-capitalism-with-cronyism.html"
image:
  placement: ""
  caption: 'Image credit: [**Getty Images/2020 Getty Images**](https://pyxis.nymag.com/v1/imgs/588/6f8/437fe858e9701768567d3f5ea238e4322f-american-airlines-coronavirus.rhorizontal.w700.jpg)'
  focal_point: ""
  preview_only: false
---

Modern-day capitalism in America is to flatten the risk curve for people who already have money by borrowing from future generations with debt-fueled bailouts for companies. We have consciously decided to reduce the downside for the wealthy, thereby limiting the upside for future generations.

Letting firms fail and share prices fall to their market level also provides younger generations with the same opportunities that boomers and Generation X were given: a chance to buy Amazon at 50 times (vs. 100 times) earnings and Brooklyn real estate at \$300 (vs. \$1,000) per square foot.

[Continue Reading](https://nymag.com/intelligencer/2020/04/scott-galloway-america-replaced-capitalism-with-cronyism.html)
