---
title: "The Threat of Inequality of Opportunity"
date: 2019-11-07
publishDate: 2019-11-07
authors: ["Shekhar Aiyar", "Christian H. Ebeke", "(IMF Blog)"]
tags:
- Equality of Opportunity
- Inequality
publication_types: ["0"]
abstract: "There are clashing views on the relationship between income inequality and growth. Some have pointed to at least some measure of inequality as a necessary outcome of the rewards to innovation and risk-taking. Others have argued that excessive income inequality depresses investment in both human and physical capital,"
featured: false
url_source: "https://blogs.imf.org/2019/11/07/the-threat-of-inequality-of-opportunity/"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: 'Image credit: [IMF Blog](https://blogs.imf.org/wp-content/uploads/2019/11/eng-oct-21-inequality.png)'
  focal_point: ""
  preview_only: false
---

There are clashing views on the relationship between income inequality and growth. Some have pointed to at least some measure of inequality as a necessary outcome of the rewards to innovation and risk-taking. Others have argued that excessive income inequality depresses investment in both human and physical capital, two key sources of long-term growth.

In recent research we argue that the crucial missing link in the inequality-growth relationship is inequality of opportunity.

[Continue Reading](https://blogs.imf.org/2019/11/07/the-threat-of-inequality-of-opportunity/)
