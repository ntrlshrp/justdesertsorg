---
title: The Economist Who Would Fix the American Dream
author: ''
date: '2019-07-17'
slug: cook-the-economist-who-would-fix-the-american-dream
categories: []
tags:
  - United States
  - Equality of Opportunity
subtitle: 'No one has done more to dispel the myth of social mobility than Raj Chetty. But he has a plan to make equality of opportunity a reality.'
summary: ''
authors: 
- Gareth Cook
- (The Atlantic)
lastmod: '2019-07-17'
featured: no
image:
  caption: 'Image credit: [Carlos Chavarría](https://cdn.theatlantic.com/assets/media/img/2019/07/18/WEL_Cook_ChettyOPener_copy_1/1920.jpg?1563459308)'
  focal_point: ''
  preview_only: no
projects: 
- opportunity-insights
url_source: https://www.theatlantic.com/magazine/archive/2019/08/raj-chettys-american-dream/592804/
---

The work that has brought Chetty such fame is an echo of his family’s history. He has pioneered an approach that uses newly available sources of government data to show how American families fare across generations, revealing striking patterns of upward mobility and stagnation. In one early study, he showed that children born in 1940 had a 90 percent chance of earning more than their parents, but for children born four decades later, that chance had fallen to 50 percent, a toss of a coin.

In 2013, Chetty released a colorful map of the United States, showing the surprising degree to which people’s financial prospects depend on where they happen to grow up. In Salt Lake City, a person born to a family in the bottom fifth of household income had a 10.8 percent chance of reaching the top fifth. In Milwaukee, the odds were less than half that.

Since then, each of his studies has become a front-page media event (“Chetty bombs,” one collaborator calls them) that combines awe—millions of data points, vivid infographics, a countrywide lens—with shock. This may not be the America you’d like to imagine, the statistics testify, but it’s what we’ve allowed America to become. Dozens of the nation’s elite colleges have more children of the 1 percent than from families in the bottom 60 percent of family income. A black boy born to a wealthy family is more than twice as likely to end up poor as a white boy from a wealthy family. Chetty has established Big Data as a moral force in the American debate.

Now he wants to do more than change our understanding of America—he wants to change America itself.

[Continue Reading](https://www.theatlantic.com/magazine/archive/2019/08/raj-chettys-american-dream/592804/)
