---
title: 'The Other American Dream: Social Mobility, Race and Opportunity'
author: ''
date: '2013-08-28'
slug: reeves-the-other-american-dream-social-mobility-race-and-opportunity
categories: []
tags:
  - Economic Mobility
  - Equality of Opportunity
  - Race
subtitle: ''
summary: ''
authors: 
- Richard V. Reeves
- (The Brookings Institution)
lastmod: '2013-08-28'
featured: no
image:
  caption: 'Image credit: [Pew Research Center](https://www.brookings.edu/wp-content/uploads/2013/08/pew_mobilitychart2.jpg?w=600&crop=0%2C0px%2C100%2C472px)'
  focal_point: ''
  preview_only: no
projects: []
url_pdf: http://www.pewstates.org/uploadedFiles/PCS_Assets/2012/Pursuing_American_Dream.pdf
url_source: https://www.brookings.edu/blog/social-mobility-memos/2013/08/28/the-other-american-dream-social-mobility-race-and-opportunity/
---

Today, President Obama is marking the 50th anniversary of the historic March on Washington, led by civil rights leader Martin Luther King, Jr. The March on Washington was for jobs, as well as freedom – indeed for the freedom that a job brings. Civil rights still have to be defended. Jobs have to be created and fairly allocated. But the most pernicious racial divide today is in social mobility: in the opportunity gap between a child born white, and a child born black.

It is true, in one sense, that [class now matters at least as much as race](http://www.washingtonmonthly.com/magazine/january_february_2013/features/the_new_white_negro042050.php?page=1). Racial gaps in education, employment and wealth reflect the disproportionate representation of black families at the bottom of the income scale. But an urgent question remains: why, in 2013, is the income distribution so skewed by race?

One answer is that black rates of upward social mobility are lower.

[Continue Reading](https://www.brookings.edu/blog/social-mobility-memos/2013/08/28/the-other-american-dream-social-mobility-race-and-opportunity/)
