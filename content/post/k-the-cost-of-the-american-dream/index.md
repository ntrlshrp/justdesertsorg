---
title: "The cost of the American dream"
author: ''
date: '2017-09-08'
slug: k-the-cost-of-the-american-dream
categories: []
tags:
  - Equality of Opportunity
  - Economic Inequality
subtitle: 'Americans, especially rich ones, underestimate the importance of where and to whom they are born'
summary: ''
authors: 
- C. K.
- (The Economist)
lastmod: '2017-09-08'
featured: no
image:
  caption: 'Image credit: [Getty Images](https://www.economist.com/sites/default/files/imagecache/640-width/20170909_USP508_0.jpg)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.economist.com/democracy-in-america/2017/09/08/the-cost-of-the-american-dream
---

PAUL RYAN, the Speaker of the House of Representatives, recently tweeted that, “[i]n our country, the condition of your birth does not determine the outcome of your life. This is what makes America so great.” The idea that every American newborn has an equal opportunity to enjoy the good life is false. But it isn’t just the Speaker who radically underestimates the importance of the lottery of birth—his is a popular view country-wide. And that is one of the reasons why debates about tax in America are so heated.

Richer parents can afford to send their children to better schools and colleges and can offer financial support for housing and other expenses. For these and a host of other reasons, where and to whom you are born is a significant determinant of life outcomes, especially in America. Miles Corak of the University of Ottawa reports that it is possible to predict nearly 50% of the variation in wages of sons in the United States by looking at the wages of their fathers a generation before. That compares to less than 20% in relatively egalitarian countries like Finland, Norway and Denmark. In America, more than half of sons born to fathers in the top decile of incomes fall no further than the eighth decile themselves, while only about half of those born to bottom decile father rise higher than the third decile. 

[Continue Reading](https://www.economist.com/democracy-in-america/2017/09/08/the-cost-of-the-american-dream)
