---
title: "Just Deserts"
date: 2015-09-15
publishDate: 2020-03-10T03:01:25.617928Z
authors: ["Claude S. Fischer", "(Boston Review)"]
publication_types: ["0"]
abstract: "Americans seem likelier than other Westerners to believe the world is fair."
featured: false
publication: ""
url_source: "https://bostonreview.net/made-america/claude-fischer-just-deserts"

image:
  caption: 'Image credit: [USDA](https://bostonreview.net/sites/default/files/snap-web.jpg)'
  focal_point: ''
  preview_only: no
---

Now that growing economic inequality is widely accepted as fact—it took a couple of decades for the stubborn to acknowledge this—some wonder why Americans are not more upset about it. Americans do not like inequality, but their dislike has not increased. This spring, 63 percent of Gallup Poll respondents agreed that “money and wealth in this country should be more evenly distributed,” but that percentage has hardly changed in thirty years. Neither widening inequality nor the Great Recession has turned Americans to the left, much less radicalized them.

[Continue Reading](https://bostonreview.net/made-america/claude-fischer-just-deserts)

