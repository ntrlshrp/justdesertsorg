---
title: "The Role of Luck in Life Success Is Far Greater Than We Realized"
date: 2018-03-01
publishDate: 2020-04-13T05:07:31.570371Z
authors: ["Scott Barry Kaufman"]
publication_types: ["0"]
abstract: "Are the most successful people in society just the luckiest people?"
featured: false
publication: "*Scientific American Blog Network*"
url_source: "https://blogs.scientificamerican.com/beautiful-minds/the-role-of-luck-in-life-success-is-far-greater-than-we-realized/"

image:
  caption: 'Image credit: [Clark Tibbs _Unsplash_](https://static.scientificamerican.com/blogs/cache/file/C287CE3C-00A1-486F-94C8E6805C6CAB43_source.jpg?w=690&h=930&C61031B3-BBCA-4946-BD88474213BCC845)'
  focal_point: ''
  preview_only: no
---

What does it take to succeed? What are the secrets of the most successful people? Judging by the popularity of magazines such as Success, Forbes, Inc., and Entrepreneur, there is no shortage of interest in these questions. There is a deep underlying assumption, however, that we can learn from them because it's their personal characteristics--such as talent, skill, mental toughness, hard work, tenacity, optimism, growth mindset, and emotional intelligence-- that got them where they are today. This assumption doesn't only underlie success magazines, but also how we distribute resources in society, from work opportunities to fame to government grants to public policy decisions. We tend to give out resources to those who have a past history of success, and tend to ignore those who have been unsuccessful, assuming that the most successful are also the most competent.

But is this assumption correct?

[Continue Reading](https://blogs.scientificamerican.com/beautiful-minds/the-role-of-luck-in-life-success-is-far-greater-than-we-realized/)
