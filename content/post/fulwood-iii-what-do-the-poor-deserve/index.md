---
title: What Do the Poor Deserve?
author: ''
date: '2012-07-24'
slug: fulwood-iii-what-do-the-poor-deserve
categories: []
tags:
  - Desert
  - Poverty
subtitle: ''
summary: ''
authors: 
- Sam Fulwood III
- (Center for American Progress)
lastmod: '2012-07-24'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.americanprogress.org/issues/race/news/2012/07/24/11867/race-and-beyond-what-do-the-poor-deserve/
---

The photograph seemingly shows a poor black child with an expensive piece of faddish technology. Such incongruity was too much for a great number of people in New Orleans to accept, said Jarvis DeBerry, who noted as much Sunday in his [Times-Picayune column](http://www.nola.com/opinions/index.ssf/2012/07/photo_of_boy_in_the_projects_w.html).

> The idea that most people in public housing are living the lush life has persisted for at least as long as presidential candidate Ronald Reagan started using the offensive “welfare queen.” But you ought to take a walk through Iberville if you think its residents are living like royalty. Walk through and see if you’d exchange their thrones for yours.

[Continue Reading](https://www.americanprogress.org/issues/race/news/2012/07/24/11867/race-and-beyond-what-do-the-poor-deserve/)
