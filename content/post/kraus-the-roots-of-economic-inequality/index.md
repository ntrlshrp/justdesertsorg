---
title: "The Roots of Economic Inequality"
date: 2018-12-01
publishDate: 2019-12-04T05:22:07.417478Z
authors: ["Michael W. Kraus", "(Yale Insights)"]
publication_types: ["0"]
abstract: "A new study shows that deeply ingrained social behaviors play a role in perpetuating economic inequality. Yale SOM’s Michael Kraus, one of the authors of the study, says we all need to mind the widening gap between rich and poor."
featured: false
url_source: "https://insights.som.yale.edu/insights/the-roots-of-economic-inequality"
---

In a paper in Advances in Experimental Social Psychology, the authors put forth a conceptual model, drawing on existing research, to show how individuals maintain inequality through their actions and beliefs. The authors look at five domains of social life—structural barriers, social class signaling, ideologies of merit, moral-relational tendencies, and intergroup processes—and detail how they perpetuate class divisions in everyday life.

[Continue Reading](https://insights.som.yale.edu/insights/the-roots-of-economic-inequality)
