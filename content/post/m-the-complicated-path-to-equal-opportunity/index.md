---
title: The complicated path to equal opportunity
author: ''
date: '2012-10-01'
slug: m-the-complicated-path-to-equal-opportunity
categories:
  - Economic Justice
tags:
  - Equality of Opportunity
  - Merit
  - United States
  - Education
subtitle: 'Should a high school for gifted New Yorkers be forced to admit less-skilled minorities?'
summary: ''
authors: 
- S. M.
- (The Economist)
lastmod: '2012-10-01'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.economist.com/democracy-in-america/2012/10/01/the-complicated-path-to-equal-opportunity
---

IN A complaint filed last week, the NAACP Legal Defense Fund charged that the admissions procedures of Stuyvesant High School and other specialised high schools in New York City are “unsound and discriminatory” because the Specialized High School Admissions Test (SHSAT)—the sole factor in admissions—has yielded striking racial imbalances in the schools’ student bodies. They are asking the federal department of education to investigate:

> Year after year, thousands of academically talented African-American and Latino students who take the test are denied admission to the Specialized High Schools at rates far higher than those for other racial groups… For example, of the 967 eighth-grade students offered admission to Stuyvesant for the 2012-13 school year, just 19 (2%) of the students were African American and 32 (3.3%) were Latino.

This complaint will likely go nowhere.

[Continue Reading](https://www.economist.com/democracy-in-america/2012/10/01/the-complicated-path-to-equal-opportunity)
