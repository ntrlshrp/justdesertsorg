---
title: Severe Inequality Is Incompatible With the American Dream
author: ''
date: '2016-12-10'
slug: semuels-severe-inequality-is-incompatible-with-the-american-dream
categories: []
tags:
  - Economic Inequality
  - Poverty
  - Equality of Opportunity
  - United States
subtitle: 'Why extreme wealth makes it hard for people to do better than their parents did'
summary: ''
authors: 
- Alana Semuels
- (The Atlantic)
lastmod: '2016-12-10'
featured: no
image:
  caption: 'Image credit: [Tony Gutierrez / AP](https://cdn.theatlantic.com/assets/media/img/mt/2016/12/AP_16258720483485/lead_720_405.jpg?mod=1533691862)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.theatlantic.com/business/archive/2016/12/equality-of-opportunity/510227/
---

The numbers are sobering: People born in the 1940s had a 92 percent chance of earning more than their parents did at age 30. For people born in the 1980s, by contrast, the chances were just 50-50.

The finding comes from a new paper out of [The Equality of Opportunity Project](http://www.equality-of-opportunity.org/), a joint research effort of Harvard and Stanford led by the economist Raj Chetty. The paper puts numbers on what many have seen firsthand for years: The American dream—the ability to climb the economic ladder and achieve more than one’s parents did—is less and less a reality with every decade that goes by.

There are two main reasons why today’s 30-somethings have a harder time than their parents did, according to the authors.

[Continue Reading](https://www.theatlantic.com/business/archive/2016/12/equality-of-opportunity/510227/)
