---
title: "50 Years Seeking Educational Equality: Revisiting the Coleman Report"
date: 2016-07-01
publishDate: 2019-12-10T05:07:51.821097Z
authors: ["Sarah D. Sparks", "(Education Week)"]
publication_types: ["0"]
abstract: "One of the most influential and hotly debated education studies in American history turns 50 this weekend. See what's changed on five key education issues in the decades since James S. Coleman published his landmark report."
featured: false
url_source: "https://www.edweek.org/ew/section/multimedia/50-years-seeking-educational-equality-the-coleman-report.html"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "Image credit: [Education Week](https://www.edweek.org/media/2016/07/01/coleman-classroomdiversity.jpg)"
  focal_point: ""
  preview_only: false
---

On this weekend in 1966, researcher James S. Coleman and his colleagues released: "Equality of Educational Opportunity." The 737-page report to Congress was dense with charts, tables, and head-poundingly complex analysis of the disparities between white and black students in public schools, and the effects of that inequity on academic achievement.

"You read the overview and you wouldn't know the dynamite that's coming up," said James McPartland, a Johns Hopkins University researcher and one of the original seven authors in Coleman’s research team.

In the decades since, what came to be known as the Coleman report has been one of the most influential and hotly debated education studies in American history. It identified families, not just schools, as key drivers of student achievement. In the heat of the Civil Rights movement, it found strong benefits of integrated schools for black and white students alike. And it included some prescient findings about how students' mindset and motivation contribute to their learning.

In the charts below, we look at what the Coleman report had to say about six key education issues—school segregation, testing, academic mindset, college enrollment, and teachers—and what we know now.

[Continue Reading](https://www.edweek.org/ew/section/multimedia/50-years-seeking-educational-equality-the-coleman-report.html)

