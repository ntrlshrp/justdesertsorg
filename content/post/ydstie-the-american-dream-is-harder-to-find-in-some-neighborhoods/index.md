---
title: The American Dream Is Harder To Find In Some Neighborhoods
author: ''
date: '2018-10-01'
slug: ydstie-the-american-dream-is-harder-to-find-in-some-neighborhoods
categories: []
tags:
  - Economic Mobility
  - Equality of Opportunity
  - Neighborhoods
subtitle: ''
summary: ''
authors: 
- John Ydstie
- (NPR | National Public Radio)
lastmod: '2018-10-01'
featured: no
image:
  caption: 'Image credit: [The Opportunity Atlas](https://media.npr.org/assets/img/2018/09/21/opportunity-map-seamus_wide-a8388226711bd93beb71bc8f956e5f0725b1c012-s1100-c15.png)'
  focal_point: ''
  preview_only: no
projects: 
- opportunity-insights
url_source: https://www.npr.org/2018/10/01/649701669/the-american-dream-is-harder-to-find-in-some-neighborhoods
---

Does the neighborhood you grow up in determine how far you move up the economic ladder?

A new online data tool being made public Monday finds a strong correlation between where people are raised and their chances of achieving the American dream.

Harvard University economist Raj Chetty has been working with a team of researchers on this tool — the first of its kind because it marries U.S. Census Bureau data with data from the Internal Revenue Service. And the findings are changing how researchers think about economic mobility.

People born in the 1940s or '50s were virtually guaranteed to achieve the American dream of earning more than their parents did, Chetty says. But that's not the case anymore.

[Continue Reading](https://www.npr.org/2018/10/01/649701669/the-american-dream-is-harder-to-find-in-some-neighborhoods)
