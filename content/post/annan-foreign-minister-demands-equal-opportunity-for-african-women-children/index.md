---
title: "Foreign Minister Demands Equal Opportunity For African Women, Children"
date: 2019-12-03
publishDate: 2019-12-10T05:46:34.963120Z
authors: ["Ekow Skare Annan", "(Front Page Ghana)"]
publication_types: ["0"]
abstract: "Minister for Foreign Affairs and Regional Integration, Shirley Ayorkor Botchwey, has indicated the need for African leaders to provide an enabling environment for the continent’s women, children and young people to flourish and reach their full potential.  She made the call when she opened the third Forum for Women Vice Chancellors in Africa symposium in Cape Coast, on Monday, December 2, 2019."
featured: false
url_source: "https://www.frontpageghana.com/2019/12/03/foreign-minister-demands-equal-opportunity-for-african-women-children/"
---

Minister for Foreign Affairs and Regional Integration, Shirley Ayorkor Botchwey, has indicated the need for African leaders to provide an enabling environment for the continent’s women, children and young people to flourish and reach their full potential.

She made the call when she opened the third Forum for Women Vice Chancellors in Africa symposium in Cape Coast, on Monday, December 2, 2019.

The 5-day symposium in Cape Coast aims at providing a platform for female leaders of higher education and other sectors in Africa to deliberate on gender equality in higher education and women’s empowerment.

According to the Foreign Minister, the growing concern regarding the lack of women in senior or leadership positions in all spheres of human endeavour, particularly in higher education, and more specifically in science and technology across Africa cannot be overemphasized. 

[Continue Reading](https://www.frontpageghana.com/2019/12/03/foreign-minister-demands-equal-opportunity-for-african-women-children/)
