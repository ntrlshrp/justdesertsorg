---
title: The American Dream, Quantified at Last
author: ''
date: '2016-12-08'
slug: leonhardt-the-american-dream-quantified-at-last
categories: []
tags:
  - Administrative Data
  - Economic Mobility
  - United States
  - Gross Domestic Product
  - Income Inequality
  - Labor and Jobs
  - Recession and Depression
  - Taxation
  - United States Economy
subtitle: ''
summary: ''
authors: 
- David Leonhardt
- (The New York Times)
lastmod: '2016-12-08'
featured: no
image:
  caption: 'Image credit: [Unknown](https://www.nytimes.com/2016/12/08/opinion/the-american-dream-quantified-at-last.html)'
  focal_point: ''
  preview_only: no
projects: 
- opportunity-insights
url_source: https://www.nytimes.com/2016/12/08/opinion/the-american-dream-quantified-at-last.html
---

The phrase “American dream” was invented during the Great Depression. It comes from a popular 1931 book by the historian James Truslow Adams, who defined it as “that dream of a land in which life should be better and richer and fuller for everyone.”

In the decades that followed, the dream became a reality. Thanks to rapid, widely shared economic growth, nearly all children grew up to achieve the most basic definition of a better life — earning more money and enjoying higher living standards than their parents had.

These days, people are arguably more worried about the American dream than at any point since the Depression. But there has been no real measure of it, despite all of the data available. No one has known how many Americans are more affluent than their parents were — and how the number has changed.

[Continue Reading](https://www.nytimes.com/2016/12/08/opinion/the-american-dream-quantified-at-last.html)
