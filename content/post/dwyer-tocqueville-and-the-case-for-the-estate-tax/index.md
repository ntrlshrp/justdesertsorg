---
title: Tocqueville and the Case for the Estate Tax
author: ''
date: '2011-04-15'
slug: dwyer-tocqueville-and-the-case-for-the-estate-tax
categories: []
tags: []
subtitle: ''
summary: ''
authors: 
- Joseph de la Torre Dwyer
- (PolicyShop)
lastmod: '2011-05-19T13:24:09-04:00'
featured: no
image:
  caption: 'Image credit: [Wikimedia Commons](https://en.wikipedia.org/wiki/File:Democracy_in_America_by_Alexis_de_Tocqueville_title_page.jpg)'
  focal_point: ''
  preview_only: no
projects: []
url_source: http://www.policyshop.net/home/2011/4/15/tocqueville-and-the-case-for-the-estate-tax.html
---

A perilous journey across the Atlantic in 1831 was made to study those who were unfree and unequal in America. This journey became, instead, a source for one of the greatest works of the modern era on liberty and equality.
With the eyes of the Old World, Alexis de Tocqueville saw as no other the novel and even shocking aspects of American society.
Among these was America’s tax policy.

In his 1835 Democracy in America, Tocqueville was amazed by America’s heightened economic and social mobility.
He wrote that rich and poor are not fixed, they "churn," and that this mobility gives incentive to rich and poor alike to strive for greater economic prosperity.
In a situation very unlike his native France, rich and poor were not fixed because inheritance tax law broke up estates.
He went so far as to [write](http://xroads.virginia.edu/%7EHyper/DETOC/ch2_18.htm), "Among a democratic people, where there is no hereditary wealth, every man works to earn a living. ... Labor is held in honor; the prejudice is not against but in its favor."

Today, America looks different. [Estate tax returns dropped](http://www.irs.gov/pub/irs-soi/10esesttaxsnap.pdf) from 110,000 in 2001 to 34,000 in 2009 largely due to increasing the filing threshold from \$675,000 to \$3.5 million.
Less than 3 out of 1,000 estates owe any estate taxes and the effective tax rate on those estates is less than 20 percent.
Making for strange bedfellows with Old World aristocrats, some Republicans such as Senator Jon Kyl want to repeal the estate tax, costing American communities [$1.3 trillion in tax revenues](http://www.cbpp.org/files/estatetaxmyths.pdf) over the next ten years.

While the income tax ought to be fair game this April 15, wealth inequalities are far greater and far more important than income inequality in determining one’s opportunities and quality of life.
According to [the seminal 1981 paper](http://sws1.bu.edu/kotlikoff/New%20Kotlikoff%20Web%20Page/The%20Role%20of%20Intergenerational%20Transfers%20in%20Aggregate%20Capital%20Formation.pdf) on intergenerational wealth transfers by Laurence Kotlikoff and Larry Summers, approximately 80 percent of each individual family’s currently existing wealth was given to them by their parents---that is, birth equals worth just as much today as it did in 19th century France.

Democracy in America, that subtle and delicate combination of liberty and equality that Tocqueville found so curiously extraordinary, requires churning of wealth among different families, communities, demographics, regions, and more.
A vibrant economy and citizenry must necessarily inherit accumulated wealth but individual wealth beyond the ordinary ought to be earned anew each generation rather than inherited from one’s parents.
Sclerotic concentrations of wealth harm the economy and the citizenry in ways Tocqueville was only too familiar with, such as two very un-American vices, the laziness of the rich and the servility of the poor.

As Americans struggle with their income tax returns and wonder if those payments match their values, it would do well to remember that one of the preeminent American values is a rejection of inherited and unearned wealth because of the ways such wealth perverts American democracy.
To continue to protect democracy, the estate tax must be revitalized so that rich become poor and poor become rich as Tocqueville perceived in 1830s America. Over the next 50 years, [$41 trillion of wealth](http://www.bc.edu/content/dam/files/research_sites/cwp/pdf/41trillionreview.pdf) will be transferred from one generation to the next, 2/3 of that by the wealthiest 7 percent who will have estates worth greater than \$1 million nominal dollars.
If America were to permanently discard the estate tax, as many suggest, it would become a nation ruled by hereditary wealth.
Tomorrow's students of democracy, inheritors of Tocqueville, would then find themselves looking elsewhere for a new "New World."
