---
title: The Radicalism of “Equal Opportunity”
author: ''
date: '2019-02-27'
slug: robinson-the-radicalism-of-equal-opportunity
categories: []
tags:
  - Economic Justice
  - Equality of Opportunity
  - Infeasible as Policy
  - Equality
subtitle: 'Having anything close to equal opportunity would require a complete global social transformation…'
summary: ''
authors: 
- Nathan J. Robinson
- (Current Affairs)
lastmod: '2019-02-27'
featured: no
image:
  caption: 'Image credit: [Unknown](https://images.currentaffairs.org/2019/02/line-218786_960_720-1024x646.jpg)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.currentaffairs.org/2019/02/the-radicalism-of-equal-opportunity/
---

You will often hear a distinction drawn between two different kinds of equality: equality of “opportunity” and equality of “outcome.” The people who draw this distinction often say that they believe in the former but not the latter. Equality of “opportunity” is desirable, but equality of “outcome” is not. As they frame it, one of these is fairly basic while the other is radical and frightening. If we were to try to ensure equal “outcomes” we would have to create a colossal social transformation. It would resemble the dystopia of Kurt Vonnegut’s “Harrison Bergeron,” in which able-bodied people were saddled with weights so that they could not dance better than disabled people, etc. The people who distinguish opportunity and outcome often do so in order to discourage us from trying to redistribute wealth from rich to poor—what matters is not whether people end up highly unequal, but whether they have the same opportunities at the start. If life is a race, it’s okay if there are “winners” and “losers” so long as the race is played fairly. “Equality of opportunity” describes the conditions under which the results of the race should be accepted: Everyone went in with the same ability to succeed, but some people came out ahead of others.

It’s very tempting to accept this framework, because it allows for a clean distinction between “capitalist” and “communist” equality. We capitalists believe that everyone should be equally able to pursue the good life, whereas the communists make the error of believing that everyone is equally entitled to the good life, and introduce all kinds of distortions and horrors in their efforts to force equality upon a highly unequal world. The wise egalitarian simply makes sure the “rules of the game” are set up fairly and doesn’t try to meddle with the outcome, even if that outcome is highly inegalitarian.

But there are severe problems with this way of looking at things.

[Continue Reading](https://www.currentaffairs.org/2019/02/the-radicalism-of-equal-opportunity/)
