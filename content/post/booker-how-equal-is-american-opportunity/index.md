---
title: How Equal Is American Opportunity? Survey Shows Attitudes Vary By Race
author: ''
date: '2015-09-21'
slug: booker-how-equal-is-american-opportunity
categories: []
tags:
  - Equality of Opportunity
  - Survey
  - Race
subtitle: ''
summary: ''
authors: 
- Brakkton Booker
- (NPR | National Public Radio)
lastmod: '2015-09-21'
featured: no
image:
  caption: 'Image credit: [Source: PBS NewsHour/Marist Poll Credit: Camila Domonoske/NPR](https://media.npr.org/assets/img/2015/09/21/divergent-views-on-fairness-of-pay_chartbuilder_custom-51cabe7239bbea3baf0f114d22176c2e92d40216-s300-c85.png)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.npr.org/sections/thetwo-way/2015/09/21/442068004/how-equal-is-american-opportunity-survey-shows-attitudes-vary-by-race
---

A new survey shows a majority of Americans, regardless of race, agree that race relations have worsened nationally in the past year — but on questions of equality, opinions were split between white and African-American respondents.

According to a PBS Newshour/Marist Poll, a racial divide still persists on how Americans view a variety of issues, including whether blacks and whites have equal opportunities of getting hired for a job, receiving a quality education and earning equal pay for equal work.

[Continue Reading](https://www.npr.org/sections/thetwo-way/2015/09/21/442068004/how-equal-is-american-opportunity-survey-shows-attitudes-vary-by-race)
