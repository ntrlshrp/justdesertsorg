---
title: Required reading to understand the tax policy fight
author: ''
date: '2017-11-13'
slug: williamson-required-reading-to-understand-the-tax-policy-fight
categories: []
tags:
  - Tax Policy
  - Values
subtitle: ''
summary: ''
authors: 
- Vanessa Williamson
- (The Brookings Institution)
lastmod: '2017-11-13'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.brookings.edu/blog/fixgov/2017/11/13/required-reading-to-understand-the-tax-policy-fight/
---

> First lesson: the top-heavy tax cuts on the policy agenda today are not the natural outcome of a widely held antipathy to taxation, or an admiration for wealthy people that is sometimes ascribed to the American public. Americans are more willing to pay taxes and are more concerned about economic inequality, than you might think.

> All of this should lead you to wonder why we’re discussing a tax bill that gives huge breaks to the wealthy and corporations as a part of a budget process proposing large cuts to popular programs. That brings us to our second lesson: politicians’ policy priorities are not determined by public opinion; they are determined by interests that have organized themselves to exert power within the party system.

> First, tax cuts have defined the Republican Party since the Reagan era, a policy commitment that is part of a broader backlash against the civil rights movement and the resulting extension of representation and public goods to African-Americans. To understand the origins of the anti-tax fervor that gripped the Republican Party starting in the late 1970s, read “Chain Reaction: The Impact of Race, Rights and Taxes on American Politics,” by Thomas Byrne Edsall and Mary D. Edsall. ...
Second, tax cuts for the rich have been pushed forward by concerted campaigns by the wealthy, as Isaac Martin explains in Rich People’s Movements: Grassroots Campaigns to Untax the 1%. Martin’s key insight is explaining how wealthy people managed to build broader constituencies for their tax cuts: by channeling frustration about other aspects of the tax code into support for policies that mostly cut rates at the very top.

[Continue Reading](https://www.brookings.edu/blog/fixgov/2017/11/13/required-reading-to-understand-the-tax-policy-fight/)
