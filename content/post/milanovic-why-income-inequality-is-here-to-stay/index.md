---
title: Why Income Inequality Is Here to Stay
author: ''
date: '2013-01-03'
slug: milanovic-why-income-inequality-is-here-to-stay
categories:
  - Economic Justice
tags:
  - Economic Inequality
  - Equality of Opportunity
  - Income Distribution
  - Inequality
subtitle: 'We keep pursuing the same policies and hoping for different results.'
summary: ''
authors: 
- Branko Milanovic
- (Harvard Business Review)
lastmod: '2013-01-03'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_source: https://hbr.org/2013/01/why-income-inequality-is-here
---

Before the global financial crisis, income inequality was relegated to the underworld of economics. The motives of those who studied it were impugned. According to Martin Feldstein, the former head of Reagan’s Council of Economic Advisors, such people must have been motivated by envy. Robert Lucas, a Nobel prize winner, thought that “nothing [is] as poisonous” to sound economics as “to focus on questions of distribution.”

But amid bailouts, unemployment, and ever-fresh financial scandals among the top 1%, the issue of income inequality has seeped into the mainstream economics and become a legitimate subject of research. Yet most of this new research simply studies the problem; the ideas on how to check rising income inequality in the United States and elsewhere seem to be remarkably few. An observer might get the impression that the long neglect in which the study of inequality has been held resulted in dearth of ideas on how economic policy should deal with it.

This is not true.

[Continue Reading](https://hbr.org/2013/01/why-income-inequality-is-here)
