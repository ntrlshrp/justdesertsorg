---
title: Is the American Dream Really Dead?
author: ''
date: '2017-01-18'
slug: dubner-is-the-american-dream-really-dead
categories:
  - American Government and Politics
tags:
  - Economic Mobility
  - United States
subtitle: ''
summary: ''
authors: 
- Stephen J. Dubner
- (Freakonomics)
lastmod: '2017-01-18'
featured: no
image:
  caption: 'Image credit: [Beth Rankin](http://freakonomics.com/wp-content/uploads/2017/01/bananas-300x215.jpg)'
  focal_point: ''
  preview_only: no
projects: []
url_source: http://freakonomics.com/podcast/american-dream-really-dead/
---

Let’s start today with a pop quiz. Here we go: in 1970, what percentage of 30-year-olds in America earned more money than their parents had earned at that age? Adjusted for inflation, of course. That’s question No. 1. And question No. 2: what percentage of American 30-year-olds today earn more than their parents earned at age 30? I’ll give you a second to think it over.

All right, you ready for the answer? The percentage of American 30-year-olds in 1970 who were earning more than their parents had earned at 30? Ninety-two percent. Isn’t that amazing? That, in a nutshell, is what we call the American Dream. And what’s the percentage now? It’s somewhere around 50 percent. Which has led some people to say this:

> Donald TRUMP: Sadly, the American Dream is dead!

[Continue Reading](http://freakonomics.com/podcast/american-dream-really-dead/)
