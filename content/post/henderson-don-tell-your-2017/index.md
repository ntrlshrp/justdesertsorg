---
title: "Don’t Tell Your Friends They’re Lucky"
date: 2017-01-19
publishDate: 2020-04-25T23:26:39.524508Z
authors: ["Bob Henderson", "(Nautilus)"]
publication_types: ["0"]
abstract: ""
featured: false
publication: ""
url_source: "http://nautil.us/issue/44/luck/dont-tell-your-friends-theyre-lucky"
image:
  placement: ""
  caption: 'Image credit: [**Ellen McCollister**](https://d3chnh8fr629l6.cloudfront.net/11442_cb1d78edc3b427d8f919a169d6931636.jpg)'
  focal_point: ""
  preview_only: false
---

The whole process of constructing life narratives is biased in ways that almost guarantee that people won’t recognize the role of chance events adequately. So, you’ve been successful, you’ve been at it 30 years. It’s true that you’ve worked hard all that time, you got up early, you put in a lot of effort, those memories are all very plentiful and available in your memory bank. You’ve solved lots of difficult problems. You remember examples of those, too. You know the formidable opponents that you’ve vanquished along the way. How can you forget them?  So, if somebody says, “Why did you succeed?” those things are going to get top billing in your story.
  
[Continue Reading](http://nautil.us/issue/44/luck/dont-tell-your-friends-theyre-lucky)
