---
title: America Is Even Less Socially Mobile Than Most Economists Thought
author: ''
date: '2015-07-23'
slug: pinsker-america-is-even-less-socially-mobile-than-most-economists-thought
categories: []
tags:
  - Economic Mobility
subtitle: 'And as a result, the policies that would address the situation are even more extreme—and more politically unfeasible.'
summary: ''
authors: 
  - Joe Pinsker
  - (The Atlantic)
lastmod: '2015-07-23'
featured: no
image:
  caption: 'Image credit: [Jonathan Ernst / Reuters](https://cdn.theatlantic.com/assets/media/img/mt/2015/07/Jonathan_Ernst/lead_720_405.jpg?mod=1533691756)'
  focal_point: ''
  preview_only: no
projects: 
  - stanford-center-on-poverty-and-inequality
url_source: https://www.theatlantic.com/business/archive/2015/07/america-social-mobility-parents-income/399311/
---

A Pew report out today tells us things about American social mobility that are new—and at the same time all too familiar.
Scads of [reports](http://www.equality-of-opportunity.org/images/mobility_geo.pdf) have documented how parents’ income dictates how financially successful someone will go on to be.
But this report suggests the effects are at the high end of previous estimates.
“One might think we’d have nailed it by now, but there was some uncertainty,” says David Grusky, the director of Stanford’s Center on Poverty and Inequality and an author of the report.

Grusky and Pablo Mitnik, his co-author and colleague at the Center on Poverty and Inequality, use a new data set provided to them by the IRS to show that in the U.S., roughly half of parental income advantages are passed onto the next generation in the form of higher earnings.

[Continue Reading](https://www.theatlantic.com/business/archive/2015/07/america-social-mobility-parents-income/399311/)
