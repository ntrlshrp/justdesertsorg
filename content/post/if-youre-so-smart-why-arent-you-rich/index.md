---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "If You're So Smart Why Aren't You Rich? Turns out it’s just chance."
subtitle: "The most successful people are not the most talented, just the luckiest, a new computer model of wealth creation confirms. Taking that into account can maximize return on many kinds of investment."
summary: ""
authors: 
- (MIT Technology Review)
tags: 
- Income Inequality
- Simulations
- Luck
categories: []
date: 2018-03-01
lastmod: 2018-03-01
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
url_source: https://www.technologyreview.com/s/610395/if-youre-so-smart-why-arent-you-rich-turns-out-its-just-chance/
---

What factors, then, determine how individuals become wealthy? Could it be that chance plays a bigger role than anybody expected? And how can these factors, whatever they are, be exploited to make the world a better and fairer place?

Today we get an answer thanks to the work of Alessandro Pluchino at the University of Catania in Italy and a couple of colleagues. These guys have created a computer model of human talent and the way people use it to exploit opportunities in life. The model allows the team to study the role of chance in this process.

The results are something of an eye-opener. Their simulations accurately reproduce the wealth distribution in the real world. But the wealthiest individuals are not the most talented (although they must have a certain level of talent). They are the luckiest.

[Continue Reading](https://www.technologyreview.com/s/610395/if-youre-so-smart-why-arent-you-rich-turns-out-its-just-chance/)
