---
title: "The radical moral implications of luck in human life"
author: ''
date: '2019-03-05'
slug: roberts-the-radical-moral-implications-of-luck-in-human-life
categories:
  - Ethics and Moral Philosophy
  - Economic Justice
tags:
  - Desert
  - Equality of Opportunity
subtitle: 'Acknowledging the role of luck is the secular equivalent of religious awakening'
summary: ''
authors: 
- David Roberts
- (Vox)
lastmod: '2019-03-05'
featured: no
image:
  caption: 'Image credit: [Christina Animashaun/Vox](https://cdn.vox-cdn.com/thumbor/yw4AI7tBBYCVgslCfuL1MAd_owI=/0x0:1800x1013/1070x602/filters:focal(756x363:1044x651)/cdn.vox-cdn.com/uploads/chorus_image/image/60963669/lead_art_draft.0.jpg)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.vox.com/science-and-health/2018/8/21/17687402/kylie-jenner-luck-human-life-moral-privilege
---

In July 2018 (when we first published this piece), there was a [minor uproar](https://www.vox.com/first-person/2018/7/14/17569650/kylie-jenner-forbes-list-2018) when Kardashian scion Kylie Jenner, who is all of 21, appeared on the cover of Forbes’s [60 richest self-made women issue](https://www.forbes.com/self-made-women/). As many people pointed out, Jenner’s success would have been impossible if she hadn’t been born white, healthy, rich, and famous. She built a successful cosmetics company---[now valued at $900 million, according to Forbes](https://www.forbes.com/sites/natalierobehmed/2019/03/05/at-21-kylie-jenner-becomes-the-youngest-self-made-billionaire-ever/?utm_source=TWITTER&utm_medium=social&utm_content=2172228804&utm_campaign=sprinklrForbesMainTwitter#237582472794)---not just with hard work but on a towering foundation of good luck.

Around the same time, there was [another minor uproar](https://www.vox.com/explainers/2018/7/26/17618264/refinery29-money-diaries-controversy) when Refinery29 published “[A Week in New York City on $25/Hour](https://www.refinery29.com/money-diary-new-york-city-marketing-intern-income),” an online diary by someone whose rent and bills are paid for by her parents. It turns out $25 an hour goes a lot further if you have no expenses!

These episodes illustrate what seems to be one of the enduring themes of our age: socially dominant groups, recipients of myriad unearned advantages, willfully refusing to acknowledge them, despite persistent efforts from socially disadvantaged groups. This is not a new theme, of course — it waxes and wanes with circumstance — but after a multi-decade rise in inequality, it has come roaring back to the fore.

[Continue Reading](https://www.vox.com/science-and-health/2018/8/21/17687402/kylie-jenner-luck-human-life-moral-privilege)
