---
title: The Transformation of the ‘American Dream’
author: ''
date: '2017-08-04'
slug: shiller-the-transformation-of-the-american-dream
categories:
  - American Government and Politics
tags:
  - Equality of Opportunity
  - Liberty
  - United States
  - Real Estate and Housing | Residential
  - United States Economy
  - United States Politics and Government
subtitle: ''
summary: ''
authors: 
- Robert J. Shiller
- (The New York Times)
lastmod: '2017-08-04'
featured: no
image:
  caption: 'Image credit: [Katherine Lam](https://static01.nyt.com/images/2017/08/04/upshot/04up-view1/merlin-to-scoop-125568809-128492-jumbo.jpg?quality=90&auto=webp)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.nytimes.com/2017/08/04/upshot/the-transformation-of-the-american-dream.html
---

“The [American Dream](https://www.usatoday.com/videos/news/nation/2017/01/30/trump:-'-american-dream-back'/97240074/) is back.” President Trump made that claim in a speech in January.

They are ringing words, but what do they mean? Language is important, but it can be slippery. Consider that the phrase, the American Dream, has changed radically through the years.

Mr. Trump and Ben Carson, the secretary of housing and urban development, have suggested it involves owning a beautiful home and a roaring business, but it wasn’t always so. Instead, in the 1930s, it meant freedom, mutual respect and equality of opportunity. It had more to do with morality than material success.

This drift in meaning is significant, because the American Dream---and international variants like [the Australian Dream](https://www.nytimes.com/2017/05/01/opinion/the-end-of-the-australian-dream.html?_r=0&module=inline), [Le Rêve Français](https://www.babelio.com/livres/Hollande-Le-reve-francais/289046) and others---represents core values.

[Continue Reading](https://www.nytimes.com/2017/08/04/upshot/the-transformation-of-the-american-dream.html)
