---
title: "America, Land of Equal Opportunity? Still Not There"
author: ''
date: '2018-04-03'
slug: smith-america-land-of-equal-opportunity-still-not-there
categories: []
tags:
  - Equality of Opportunity
  - United States
  - Race
  - Education
  - Equality
  - France
  - Gender
  - Italy
  - John H Friedman
  - Sweden
  - Wealth
  - Women
subtitle: 'One group is still uniquely disadvantaged: black men.'
summary: ''
authors: 
- Noah Smith
- (Bloomberg)
lastmod: '2018-04-03'
featured: no
image:
  caption: 'Image credit: [Chetty, Hendren, Jones, Porter](https://assets.bwbx.io/images/users/iqjWHBFdfxIU/iH1tEAFKT5zQ/v0/600x-1.jpg)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.bloomberg.com/opinion/articles/2018-04-03/u-s-doesn-t-deliver-on-promise-of-equal-opportunity-for-all
url_pdf: http://www.equality-of-opportunity.org/assets/documents/race_paper.pdf
---

Lack of opportunity is a huge source of economic and social dissatisfaction. Income and wealth inequality aren’t pleasant, and many people want some redistribution, but most seem to accept that luck, drive and natural advantages inevitably create some degree of inequality. But when people feel like they don’t have a chance to move up in the world even if they try hard and do all the right things, that’s when they break out the rakes and pitchforks and storm the castle.

That’s the conclusion of a recent paper by economists Alberto Alesina, Stefanie Stantcheva and Edoardo Teso, anyway. Using surveys in France, Italy, Sweden, the U.K. and the U.S., they found that people who believed their society offered less opportunity for people to climb up the economic ladder also tended to support more government intervention to increase equality of opportunity (such as higher education spending). When the researchers presented respondents with information about low mobility, their desire for redistribution rose.

One of the most glaringly unfair impediments to opportunity is racism.

[Continue Reading](https://www.bloomberg.com/opinion/articles/2018-04-03/u-s-doesn-t-deliver-on-promise-of-equal-opportunity-for-all)
