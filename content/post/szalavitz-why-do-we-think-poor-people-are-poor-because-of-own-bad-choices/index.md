---
title: Why Do We Think Poor People are Poor Because of Their Own Bad Choices?
author: ''
date: '2017-07-05'
slug: szalavitz-why-do-we-think-poor-people-are-poor-because-of-their-own-bad-choices
categories:
  - Economic Justice
tags:
  - Economic Justice
  - Equality of Opportunity
  - Poverty
  - Psychology
subtitle: ''
summary: ''
authors: 
- Maia Szalavitz
- (Economic Hardship Reporting Project)
lastmod: '2017-07-05'
featured: no
image:
  caption: '‘Among the wealthy, biases allow society’s winners to believe that they got where they are by hard work alone’. Illustration: Rosie Roberts'
  focal_point: ''
  preview_only: no
projects: 
- economic-hardship-reporting-project
url_source: http://economichardship.org/archive//why-do-we-think-poor-people-are-poor-because-of-their-own-bad-choices
---

Cecilia Mo thought she knew all about growing up poor when she began teaching at Thomas Jefferson senior high school in south Los Angeles. As a child, she remembered standing in line, holding a free lunch ticket. But it turned out that Mo could still be shocked by poverty and violence – especially after a 13-year-old student called her in obvious panic. He had just seen his cousin get shot in his front yard.

For Mo, hard work and a good education took her to Harvard and Stanford. But when she saw just how much chaos and violence her LA students faced, she recognized how lucky she had been growing up with educated parents and a safe, if financially stretched, home.

Now, as an assistant professor of public policy and education at Vanderbilt University, Mo studies how to get upper-class Americans to recognize the advantages they have. She is among a group of scholars trying to understand how rich and poor alike justify inequality. What these academics are finding is that the American dream is being used to rationalize a national nightmare.

It all starts with the psychology concept known as the “fundamental attribution error”. This is a natural tendency to see the behavior of others as being determined by their character – while excusing our own behavior based on circumstances.

[Continue Reading](http://economichardship.org/archive//why-do-we-think-poor-people-are-poor-because-of-their-own-bad-choices)
