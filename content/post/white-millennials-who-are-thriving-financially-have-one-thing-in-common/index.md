---
title: Millennials Who Are Thriving Financially Have One Thing in Common
author: ''
date: '2015-07-15'
slug: white-millennials-who-are-thriving-financially-have-one-thing-in-common
categories: []
tags:
  - Children
  - Intergenerational Transfers
subtitle: '... Rich parents.'
summary: ''
authors: 
  - Gillian B. White
  - (The Atlantic)
lastmod: '2015-07-15'
featured: no
image:
  caption: 'Image credit: [Jessica Rinaldi / Reuters](https://cdn.theatlantic.com/assets/media/img/mt/2015/07/RTR32D90/lead_720_405.jpg?mod=1533691755)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.theatlantic.com/business/archive/2015/07/millennials-with-rich-parents/398501/
---

Millions of America’s young people are really struggling financially.
Around 30 percent are living with their parents, and many others are coping with stagnant wages, underemployment, and sky-high rent.
And then there are those who are doing just great—owning a house, buying a car, and consistently putting money away for retirement.
These, however, are not your run-of-the-mill Millennials. Nope. These Millennials have something very special: rich parents.

[Continue Reading](https://www.theatlantic.com/business/archive/2015/07/millennials-with-rich-parents/398501/)
