---
title: What the Poor Deserve
author: ''
date: '1995-10-22'
slug: frankel-what-the-poor-deserve
categories: []
tags:
  - Desert
  - Poverty
subtitle: ''
summary: ''
authors: 
- Max Frankel
- (The New York Times)
lastmod: '1995-10-22'
featured: no
image:
  caption: 'Image credit: [The New York Times Archives](https://s1.nyt.com/timesmachine/pages/1/1995/10/22/961095_360W.png?quality=75&auto=webp&disable=upscale)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.nytimes.com/1995/10/22/magazine/word-image-what-the-poor-deserve.html
---

THE STARK WORD FROM WASHINGTON IS that America's War on Poverty has been totally transformed. It is now a War on the Poor.

Who's to blame? In the view of Prof. Herbert J. Gans, a Columbia sociologist and perceptive critic of American life, almost all the nonpoor must share the blame. Despite the New Deal and what he calls the Great Society's "Skirmish on Poverty," Americans have for much of their history demonized the poor as undeserving shirkers and cheats. And after poring over the literature, journalism and scholarship about poverty, he is especially critical of wordsmiths and image makers for spreading stereotypes that stigmatize the poor and question their morality and values.

[Continue Reading](https://www.nytimes.com/1995/10/22/magazine/word-image-what-the-poor-deserve.html)
