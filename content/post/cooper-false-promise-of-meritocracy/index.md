---
title: The False Promise of Meritocracy
author: ''
date: '2015-12-01'
slug: cooper-the-false-promise-of-meritocracy
categories:
  - Economic Justice
tags:
  - Equality of Opportunity
  - Race
  - Sex
  - Discrimination
subtitle: 'Managers who believe themselves to be fair and objective judges of ability often overlook women and minorities who are deserving of job offers and pay increases.'
summary: ''
authors: 
- Marianne Cooper
- (The Atlantic)
lastmod: '2015-12-01'
featured: no
image:
  caption: 'Image credit: [Michael Buholzer / Reuters](https://cdn.theatlantic.com/assets/media/img/mt/2015/11/RTR3BT5M/lead_720_405.jpg?mod=1533691789)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.theatlantic.com/business/archive/2015/12/meritocracy/418074/
---

Americans are, [compared](http://esr.oxfordjournals.org/content/23/5/649.full.pdf+html) with populations of other countries, particularly enthusiastic about the idea of meritocracy, a system that rewards merit ([ability + effort](http://www.transactionpub.com/title/The-Rise-of-the-Meritocracy-978-1-56000-704-3.html)) with success. Americans are [more likely to believe](http://www.brookings.edu/~/media/Research/Files/Reports/2008/2/economic-mobility-sawhill/02_economic_mobility_sawhill_ch3.PDF) that people are rewarded for their intelligence and skills and are less likely to believe that family wealth plays a key role in getting ahead. And Americans’ support for meritocratic principles has [remained stable](http://www.sciencedirect.com/science/article/pii/S0276562414000122) over the last two decades despite growing economic inequality, recessions, and the fact that there is [less mobility](http://www.brookings.edu/~/media/Research/Files/Reports/2008/2/economic-mobility-sawhill/02_economic_mobility_sawhill_ch3.PDF) in the United States than in most other industrialized countries.

This strong commitment to meritocratic ideals can lead to suspicion of efforts that aim to support particular demographic groups. For example, [initiatives](http://www.fastcompany.com/3052518/strong-female-lead/can-this-change-eliminate-the-stem-gender-gap) designed to recruit or provide development opportunities to under-represented groups often come under attack as “reverse discrimination.” Some companies even [justify](https://bullfrogheather.wordpress.com/2015/06/03/board-diversity/) not having diversity policies by highlighting their commitment to meritocracy. If a company evaluates people on their skills, abilities, and merit, without consideration of their gender, race, sexuality etc., and managers are objective in their assessments then there is no need for diversity policies, the thinking goes.

But is this true? Do commitments to meritocracy and objectivity lead to more fair workplaces?

[Continue Reading](https://www.theatlantic.com/business/archive/2015/12/meritocracy/418074/)
