---
title: "Poverty and equality of opportunity: three pictures to motivate policy for social mobility"
date: 2019-11-01
publishDate: 2019-12-04T04:08:43.361278Z
authors: ["Miles Corak", "(Economics for public policy)"]
publication_types: ["0"]
abstract: "Read my comments presented to the Public Economics Forum on “Intergenerationally Disadvantaged: Newest Evidence and What it Means for Policy,” organized by the Melbourne Institute for Applied Economic and Social Research, on November 26th, 2019 in Canberra, Australia."
featured: false
url_source: "https://milescorak.com/2019/11/25/poverty-and-equality-of-opportunity-three-pictures-to-motivate-policy-for-social-mobility/"
---

Research using the variation of social mobility within countries like the United States and Canada shows that intergenerational cycles of low income are more likely in communities that have more bottom half inequality, the correlation with overall inequality and with top end inequality being much weaker. Upward mobility is easier when the poorest incomes are not that far off from middle incomes.

The bottom line for public policy is don’t let inequality increase in the bottom half of the income distribution, indeed strive to reduce it in a way that encourages labour market and social engagement.

[Continue Reading](https://milescorak.com/2019/11/25/poverty-and-equality-of-opportunity-three-pictures-to-motivate-policy-for-social-mobility/)
