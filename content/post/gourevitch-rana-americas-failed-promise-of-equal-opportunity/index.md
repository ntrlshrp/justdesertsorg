---
title: America's failed promise of equal opportunity
author: ''
date: '2012-02-12'
slug: gourevitch-rana-america-s-failed-promise-of-equal-opportunity
categories:
  - American Government and Politics
tags:
  - Democracy
  - Equality of Opportunity
  - United States
subtitle: 'To achieve a truly fair society, we need to look to Lincoln, not Jefferson'
summary: ''
authors: 
- Alex Gourevitch
- Aziz Rana
- (Salon)
lastmod: '2012-02-12'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.salon.com/2012/02/12/americas_failed_promise_of_equal_opportunity/
---

Americans are increasingly aware that the ideal of equal opportunity is a false promise, but neither party really seems to get it.

Republicans barely admit the problem exists, or if they do, they think tax cuts are the answer. All facts point in the opposite direction. Despite various tax cuts over the past 30 years, not only have income and wealth inequality dramatically increased, but the ability of individuals to rise out of their own class has declined. Social stagnation is increasingly the norm, with poverty rates the highest in 15 years, real wage gains worse even than during the decade of the Great Depression, average earnings barely above what they were 50 years ago, and more than 80 percent of the income growth of the past 25 years going to the top 1 percent. In fact, since 1983, the bottom 40 percent of households have seen real declines in their income and the same goes for the bottom 60 percent when it comes to wealth. We know what the economic status quo does: It redistributes upwards.

Despite the ambiguity of their goals, the Occupy protests have made one point abundantly clear: The mainstream Democratic alternative is paltry stuff. For the most part, Democrats disagree that tax cuts and deregulation are the solution, and instead argue that the state should be used to guarantee equal opportunity. For instance, cheap, publicly available education, job training and affirmative action are all justified on the grounds that each American should have the skills to compete and the labor market should treat everyone equally.

Yet, the two parties differ only on means, not ends. While Republicans profess a more abiding faith in a self-regulating economy, Democrats believe carefully tailored state interventions are needed to ensure equal opportunity.

The question becomes: Equal opportunity for what?

[Continue Reading](https://www.salon.com/2012/02/12/americas_failed_promise_of_equal_opportunity/)
