---
title: "The Equality Conundrum"
subtitle: We all agree that inequality is bad. But what kind of equality is good?
date: 2020-01-06
publishDate: 2020-02-24T22:51:39.107715Z
authors: ["Joshua Rothman", "(The New Yorker)"]
tags:
- Equality
- Egalitarianism
publication_types: ["0"]
abstract: "The complexities of egalitarianism are especially frustrating because inequalities are so easy to grasp. C.E.O.s, on average, make almost three hundred times what their employees make; billionaire donors shape our politics; automation favors owners over workers; urban economies grow while rural areas stagnate; the best health care goes to the richest. Across the political spectrum, we grieve the loss of what Alexis de Tocqueville called the “general equality of conditions,” which, with the grievous exception of slavery, once shaped American society. It’s not just about money. Tocqueville, writing in 1835, noted that our “ordinary practices of life” were egalitarian, too: we behaved as if there weren’t many differences among us. Today, there are “premiere” lines for popcorn at the movies and five tiers of Uber; we still struggle to address obvious inequalities of all kinds based on race, gender, sexual orientation, and other aspects of identity. Inequality is everywhere, and unignorable. We’ve diagnosed the disease. Why can’t we agree on a cure?"
featured: false
url_source: "https://www.newyorker.com/magazine/2020/01/13/the-equality-conundrum"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: 'Image credit: [The New Yorker](https://media.newyorker.com/photos/5e0e8c8065900a00082135d9/master/w_2560%2Cc_limit/200113_r35667.jpg)'
  focal_point: ""
  preview_only: false
---

The complexities of egalitarianism are especially frustrating because inequalities are so easy to grasp. C.E.O.s, on average, make almost three hundred times what their employees make; billionaire donors shape our politics; automation favors owners over workers; urban economies grow while rural areas stagnate; the best health care goes to the richest. Across the political spectrum, we grieve the loss of what Alexis de Tocqueville called the “general equality of conditions,” which, with the grievous exception of slavery, once shaped American society. It’s not just about money. Tocqueville, writing in 1835, noted that our “ordinary practices of life” were egalitarian, too: we behaved as if there weren’t many differences among us. Today, there are “premiere” lines for popcorn at the movies and five tiers of Uber; we still struggle to address obvious inequalities of all kinds based on race, gender, sexual orientation, and other aspects of identity. Inequality is everywhere, and unignorable. We’ve diagnosed the disease. Why can’t we agree on a cure?

[Continue Reading](https://www.newyorker.com/magazine/2020/01/13/the-equality-conundrum)
