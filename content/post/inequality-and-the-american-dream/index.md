---
title: Inequality and the American Dream
author: ''
date: '2006-06-15'
slug: inequality-and-the-american-dream
categories:
  - Economic Justice
tags:
  - Equality of Opportunity
  - Economic Mobility
subtitle: "The world's most impressive economic machine needs a little adjusting"
summary: ''
authors: 
- (The Economist)
lastmod: '2006-06-15'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.economist.com/leaders/2006/06/15/inequality-and-the-american-dream
---

MORE than any other country, America defines itself by a collective dream: the dream of economic opportunity and upward mobility. Its proudest boast is that it offers a chance of the good life to everybody who is willing to work hard and play by the rules. This ideal has made the United States the world's strongest magnet for immigrants; it has also reconciled ordinary Americans to the rough side of a dynamic economy, with all its inequalities and insecurities. Who cares if the boss earns 300 times more than the average working stiff, if the stiff knows he can become the boss?

Yet many people feel unhappy about the American model—not least in the United States.

[Continue Reading](https://www.economist.com/leaders/2006/06/15/inequality-and-the-american-dream)
