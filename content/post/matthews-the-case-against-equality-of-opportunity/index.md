---
title: The case against equality of opportunity
author: ''
date: '2015-09-21'
slug: matthews-the-case-against-equality-of-opportunity
categories:
  - Ethics and Moral Philosophy
tags: 
- Equality of Opportunity
- Equality
subtitle: "It's an incoherent, impossible ideal. And if we're really going to fight inequality, it needs to be abandoned."
summary: ''
authors: 
- Dylan Matthews
- (Vox)
lastmod: '2015-09-21'
featured: yes
image:
  caption: 'Image credit: [Javier Zarracina / Vox](https://cdn.vox-cdn.com/thumbor/oFo5SfcpdQCwOv0pJNshx7Z0R68=/297x0:1917x1215/1070x602/filters:focal(297x0:1917x1215)/cdn.vox-cdn.com/uploads/chorus_image/image/47195218/stairs_illo.0.0.jpg)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.vox.com/2015/9/21/9334215/equality-of-opportunity
---

Everyone wants equality of opportunity. It is not a subject of political debate, but the precondition of political debate. Promises to achieve equality of opportunity, like promises to create jobs or protect America abroad, are the white noise of campaign season, drawing neither notice nor challenge. Respected think tanks like the Brookings Institution establish entire projects devoted to figuring out how to advance equality of opportunity. Raj Chetty and Emmanuel Saez, two of the best microeconomists of their generation, have joined forces to start the Equality of Opportunity Project, which is meant to produce hard numbers about opportunity across time and across regions. Huge amounts of time, money, and intellectual effort are devoted to this idea, that a just world is one in which opportunity is equal, even if outcomes aren't.

The only problem? No one really wants equality of opportunity, nor anything close to it. Nor should they. Pursuing true equality of opportunity would require turning America into a dystopian, totalitarian nightmare — and even then, it would still prove impossible.

[Continue Reading](https://www.vox.com/2015/9/21/9334215/equality-of-opportunity)
