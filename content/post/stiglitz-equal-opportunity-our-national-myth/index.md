---
title: Equal Opportunity, Our National Myth
author: ''
date: '2013-02-16'
slug: stiglitz-equal-opportunity-our-national-myth
categories:
  - Economic Justice
tags:
  - Equality of Opportunity
  - The Great Divide
subtitle: ''
summary: ''
authors: 
- Joseph E. Stiglitz
- (The New York Times)
lastmod: '2013-02-16'
featured: no
image:
  caption: 'Image Credit: [Mark Pernice](https://static01.nyt.com/images/2013/02/17/opinion/17greatdivide-img/17greatdivide-img-tmagArticle.gif)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://opinionator.blogs.nytimes.com/2013/02/16/equal-opportunity-our-national-myth/
---

President Obama’s second Inaugural Address used soaring language to reaffirm America’s commitment to the dream of equality of opportunity: “We are true to our creed when a little girl born into the bleakest poverty knows that she has the same chance to succeed as anybody else, because she is an American; she is free, and she is equal, not just in the eyes of God but also in our own.”

The gap between aspiration and reality could hardly be wider. Today, the United States has less equality of opportunity than almost any other advanced industrial country. Study after study has exposed the myth that America is a land of opportunity. This is especially tragic: While Americans may differ on the desirability of equality of outcomes, there is near-universal consensus that inequality of opportunity is indefensible. The Pew Research Center has found that some 90 percent of Americans believe that the government should do everything it can to ensure equality of opportunity.

Perhaps a hundred years ago, America might have rightly claimed to have been the land of opportunity, or at least a land where there was more opportunity than elsewhere. But not for at least a quarter of a century. Horatio Alger-style rags-to-riches stories were not a deliberate hoax, but given how they’ve lulled us into a sense of complacency, they might as well have been.

[Continue Reading](https://opinionator.blogs.nytimes.com/2013/02/16/equal-opportunity-our-national-myth/)
