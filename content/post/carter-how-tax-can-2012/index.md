---
title: "How tax can reduce inequality"
date: 2012-01-01
publishDate: 2020-03-10T03:01:25.622779Z
authors: ["Alan Carter", "Stephen Matthews"]
publication_types: ["2"]
abstract: "A rising tide may not now lift all boats, to misquote US President Kennedy’s original analogy made in 1963 linking economic growth to prosperity for all. Can governments maintain the social cohesion needed for sustainable, long-term growth? Supporting an equitable income distribution remains one of the key goals of fiscal (and tax) policy."
featured: false
publication: ""
tags: ["Tax", "Countries", "OECD", "G20"]
url_source: "http://oecdobserver.org/news/fullstory.php/aid/3782/How_tax_can_reduce_inequality.html"

image:
  caption: 'Image Credit: [OECD](http://oecdobserver.org/files/290_291/TaxChartFullCountriesLARGE.jpg)'
  focal_point: ''
  preview_only: no
---

The rapid growth of emerging economies in the past decade or so has lifted hundreds of millions of people out of absolute poverty and reduced income disparities across the world as a whole. At the same time, until the financial and economic crisis of 2008, most other economies were expanding too. However, within the OECD and emerging economies not all regions or people benefitted equally from the growth years. On the contrary, the distribution of income tended to become more unequal.

Unsurprisingly, particularly since the onset of the crisis, these trends have increased the salience of “fairness” in political debate in many countries, in terms of both equality of opportunity and of outcomes for household incomes and consumption. While few doubt that fairness is important, interpretations of what is fair differ and may in part reflect historical norms for the distribution of income, which can differ widely between countries (see chart). That said, over the longer term too much inequality may be inimical to growth.

Tax policy can play a major role in making the post-tax income distribution less unequal. In addition, tax policy is crucial for raising revenues to finance public expenditure on transfers, health and education that tend to favour low-income households, as well as on growth-enabling infrastructure that can also increase social equity.

[Continue Reading](https://oecdobserver.org/news/fullstory.php/aid/3782/How_tax_can_reduce_inequality.html)
