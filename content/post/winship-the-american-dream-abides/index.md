---
title: The American Dream Abides
author: ''
date: '2017-05-15'
slug: winship-the-american-dream-abides
categories:
  - Economic Justice
tags:
  - Economic Mobility
  - Equality of Opportunity
  - United States
subtitle: 'Social mobility is still going strong in the Land of Opportunity.'
summary: ''
authors: 
- Scott Winship
- (National Review)
lastmod: '2017-05-15'
featured: no
image:
  caption: 'Image credit: [Mylightscapes/Dreamstime](https://i2.wp.com/www.nationalreview.com/wp-content/uploads/2017/05/american-dream-social-mobility-opportunity-are-alive-well.jpg?fit=788%2C460&ssl=1)'
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.nationalreview.com/2017/05/american-dream-social-mobility-opportunity/
---

Is the American dream on life support? That’s the perennial claim of “declinists,” who are convinced that the American spirit of opportunity is at death’s door. That claim was recently bolstered by research from a team of top economists, who found that half of today’s 30-year-olds are worse off than their parents were at the same age. A closer look at that study, however, reveals that opportunity is alive and well.

[Continue Reading](https://www.nationalreview.com/2017/05/american-dream-social-mobility-opportunity/)
