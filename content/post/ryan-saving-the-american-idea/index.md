---
title: 'Saving the American Idea: Rejecting Fear, Envy, and the Politics of Division'
author: ''
date: '2011-11-14'
slug: ryan-saving-the-american-idea-rejecting-fear-envy-and-the-politics-of-division
categories:
  - American Government and Politics
tags:
  - Desert
  - Economic Inequality
  - Economic Justice
  - Equality of Opportunity
  - Merit
  - Responsibility
  - United States
subtitle: 'The American Idea is that justice is done when we level the playing field at the starting line and rewards are proportionate to merit and effort.'
summary: ''
authors: 
- Honorable Paul Ryan
- (The Heritage Foundation)
lastmod: '2011-11-14'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_pdf: https://thf_media.s3.amazonaws.com/2011/pdf/hl1196.pdf
---

The American commitment to equality of opportunity, economic liberty, and upward mobility is not tried in days of prosperity. It is tested when times are tough---when fear and envy are used to divide Americans and further the interests of politicians and their cronies. In this major address at The Heritage Foundation, Congressman Paul Ryan dissects the real class warfare---a class of governing elites, exploiting the politics of division to pick winners and losers in our economy and determine our destinies for us---and outlines a principled, pro-growth alternative to this path of debt, doubt and decline.
