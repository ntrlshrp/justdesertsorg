---
title: "The reality of income inequality in America"
date: 2019-10-27
publishDate: 2019-12-10T05:15:39.537669Z
authors: ["Mike Rosen", "(Colorado Springs Gazette)"]
publication_types: ["0"]
abstract: "In a political auction for seductive but unachievable outcomes at someone else’s expense, socialist and progressive politicians unconstrained by economic reality can always outbid conservatives."
featured: false
url_source: "https://gazette.com/opinion/column-the-reality-of-income-inequality-in-america/article_e75c29fe-f6b1-11e9-8913-a7c898ef2528.html"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
---

In a political auction for seductive but unachievable outcomes at someone else’s expense, socialist and progressive politicians unconstrained by economic reality can always outbid conservatives. Bernie Sanders’ utopian ravings and Elizabeth Warren’s cornucopia of extravagant “plans” magically funded by a new tax on “wealth” (on top of sharp increases in taxes on income) is this election season’s theme.

Their rallying cry is the evil of “income inequality.” But income inequality is not evil, it’s unavoidable and an essential element of a market economy and a free society.

[Continue Reading](https://gazette.com/opinion/column-the-reality-of-income-inequality-in-america/article_e75c29fe-f6b1-11e9-8913-a7c898ef2528.html)
