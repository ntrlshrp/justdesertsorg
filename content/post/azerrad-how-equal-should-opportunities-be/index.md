---
title: How Equal Should Opportunities Be?
author: ''
date: '2016-06-01'
slug: azerrad-how-equal-should-opportunities-be
categories:
  - American Government and Politics
  - Economic Justice
tags:
  - Equality of Opportunity
  - Infeasible as Policy
  - Equality
subtitle: ''
summary: ''
authors: 
- David Azerrad
- (National Affairs)
lastmod: '2016-06-01'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
url_source: https://www.nationalaffairs.com/publications/detail/how-equal-should-opportunities-be
---

In 1864, Abraham Lincoln gave a short speech to a sanitary fair in Baltimore. While the address itself is of little note, it does contain a passage that sheds much light on the character of political debates in America. "The world has never had a good definition of the word liberty," said Lincoln, "and the American people, just now, are much in want of one. We all declare for liberty; but in using the same word we do not all mean the same thing."

What Lincoln said of liberty applies equally to our other core political ideals. In America, we don't disagree about whether equality, rights, and democracy are good. We disagree about what they entail. And so, when it comes to "equality of opportunity"---perhaps the most cherished term in our political lexicon---nearly all Americans embrace the idea. About 95% of us agree that "everyone in America should have an equal opportunity to get ahead." But we don't all understand this to mean the same thing. We differ widely about how to gauge whether opportunities are indeed equal, and over how much should be done---and by whom---to equalize opportunities. At its root, our ongoing debate about the vitality of the American Dream and the promise of equality of opportunity underlying it is really a debate over what life owes us and what we owe one another.

Because equality of opportunity looms so large over our politics, it is crucial that we better understand its different meanings. Conservatives, in particular, need to gain clarity about the left's expansive view of equality of opportunity as equality of life chances---arguably the central moral doctrine of modern liberalism. To be sure, it is a powerful idea because it appeals directly to our democratic prejudices. The challenge we face is how to resist this siren song and its damaging implications, while continuing to champion sound policies that can expand opportunity for those who were born into challenging circumstances.

[Continue Reading](https://www.nationalaffairs.com/publications/detail/how-equal-should-opportunities-be)
