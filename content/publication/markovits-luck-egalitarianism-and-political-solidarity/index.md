---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Luck Egalitarianism and Political Solidarity"
authors: 
- Daniel Markovits
date: 2008-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-12T18:29:19-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Theoretical Inquiries in Law"
publication_short: ""

abstract: "Luck egalitarianism---the theory that makes individual responsibility central to distributive justice, so that bad luck underwrites a more compelling case for redistribution than do the bad choices of the disadvantaged---has recently come under a sustained attack from critics who are deeply committed to the broader struggle for equality. These egalitarian critics object, first, that luck egalitarianism’s policy recommendations are often unappealing. Second, they add that luck egalitarianism neglects the deep political connection between equality and non-subordination, in favor of a shallowly distributive regime.

This Article argues that both objections to luck egalitarianism have been exaggerated. Insofar as the criticisms are accurate, they apply only to a particular, maximalist strand of luck egalitarianism, whose distributive principle does not merely adjust allocations in light of responsibility but instead proposes that allocations should precisely track responsibility. However, this responsibility-tracking view does not represent the best or truest development of the basic luck egalitarian ideal.

Moreover, the pathologies of the responsibility-tracking view help to cast the appeal of more judicious luck egalitarianism into sharp relief. The redistributive policies that more moderate developments of luck egalitarianism recommend are less objectionable than critics have supposed. And, more importantly, such modest luck egalitarianism is not a purely distributive ideal but instead contains, at its core, a vision of political solidarity among free and equal citizens."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Luck-Egalitarianism
- Solidarity
categories: 
- Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://www.degruyter.com/view/j/til.2007.9.issue-1/til.2007.9.1.1176/til.2007.9.1.1176.xml
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
