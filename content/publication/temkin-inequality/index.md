---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Inequality"
authors: 
- Larry S. Temkin
date: 1993-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-03T20:54:51-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Equality has long been among the most potent of human ideals and it continues to play a prominent role in political argument. Views about equality inform much of the debate about wide-ranging issues such as racism, sexism, obligations to the poor or handicapped, relations between developed and developing countries, and the justification of competing political, economic, and ideological systems. Temkin begins his illuminating examination with a simple question: when is one situation worse than another regarding inequality? In exploring this question, a new approach to understanding inequality emerges. Temkin goes against the common view that inequality is simple and holistic and argues instead that it is complex, individualistic, and essentially comparative. He presents a new way of thinking about equality and inequality that challenges the assumptions of philosophers, welfare economists, and others, and has significant and far-reaching implications on a practical as well as a theoretical level."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Egalitarianism
categories: 
- Economic Justice
- Ethics and Moral Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: "https://global.oup.com/academic/product/inequality-9780195078602?lang=en&cc=us#"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
