---
title: "Intergenerational mobility, intergenerational effects, sibling correlations, and equality of opportunity: a comparison of four approaches"
date: 2020-02-01
publishDate: 2020-02-24T23:15:46.713155Z
authors: ["Anders Björklund", "Markus Jäntti"]
publication_types: ["3"]
abstract: "This paper presents and discusses four different approaches to the study of how individuals’ income and education during adulthood are related to their family background. The most well-known approach, intergenerational mobility, describes how parents’ and offspring’s income or education are related to each other. The intergenerational-effect literature addresses the question how an intervention that changes parental income or education causally affects their children’s outcome. The sibling-correlation approach estimates the share of total inequality that is attributed to factors shared by siblings. This share is generally substantially higher than what is revealed by intergenerational mobility estimates. Finally, the equality-of-opportunity approach is looking for a set of factors, in the family background and otherwise, that are important for children’s outcomes and that children cannot be held accountable for.  We argue that all four approaches are most informative and that recent research has provided insightful results. However, by comparing results from the different approaches, it is possible to paint a more nuanced picture of the role of family background. Thus, we recommend that scholars working in the four subfields pay more attention to each other’s research."
featured: false
publication: "Research in Social Stratification and Mobility"
tags: ["intergenerational mobility", "intergenerational effects", "sibling correlations", "inequality of opportunity"]
url_pdf: "https://www.sciencedirect.com/science/article/pii/S0276562419301544"
doi: "10.1016/j.rssm.2019.100455"
---

