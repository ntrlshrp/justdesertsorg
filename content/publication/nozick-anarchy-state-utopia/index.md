---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Anarchy, State, and Utopia"
authors: 
- Robert Nozick
date: 2013-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-03T20:54:13-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "First published in response to John Rawls’ A Theory of Justice, Robert Nozick’s Anarchy, State, and Utopia has become a defining text of classic libertarian thought. Challenging and ultimately rejecting liberal, socialist, and conservative agendas, Nozick boldly asserts that the rights of individuals are violated as a state’s responsibilities increase-and that the only way to avoid these violations is the creation of a minimalist state limited to the enforcement of contracts and to protection against force, fraud, and theft.

Translated into 100 languages, winner of the National Book Award, and named one of the 100 Most Influential Books since World War II by the Times Literary Supplement, Anarchy, State and Utopia remains one of the most theoretically trenchant and philosophically rich defenses of economic liberalism to date. With an introduction by philosopher Thomas Nagel, this edition brings Nozick and his work to a new generation of readers."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Right Libertarianism
- Consent
- Civil rights
- Anarchism
- Human rights
- Utopias
- The State
- Basic Equality
- Exploitation
categories: 
- Economic Justice
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: "https://www.basicbooks.com/titles/robert-nozick/anarchy-state-and-utopia/9780465051007/"
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
