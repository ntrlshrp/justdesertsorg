---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Success and Luck: Good Fortune and the Myth of Meritocracy"
authors: 
- Robert H. Frank
date: 2016-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-02T13:16:27-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "How important is luck in economic success? No question more reliably divides conservatives from liberals. As conservatives correctly observe, people who amass great fortunes are almost always talented and hardworking. But liberals are also correct to note that countless others have those same qualities yet never earn much. In recent years, social scientists have discovered that chance plays a much larger role in important life outcomes than most people imagine. In Success and Luck, bestselling author and New York Times economics columnist Robert Frank explores the surprising implications of those findings to show why the rich underestimate the importance of luck in success—and why that hurts everyone, even the wealthy.

Frank describes how, in a world increasingly dominated by winner-take-all markets, chance opportunities and trivial initial advantages often translate into much larger ones—and enormous income differences—over time; how false beliefs about luck persist, despite compelling evidence against them; and how myths about personal success and luck shape individual and political choices in harmful ways.

But, Frank argues, we could decrease the inequality driven by sheer luck by adopting simple, unintrusive policies that would free up trillions of dollars each year—more than enough to fix our crumbling infrastructure, expand healthcare coverage, fight global warming, and reduce poverty, all without requiring painful sacrifices from anyone. If this sounds implausible, you'll be surprised to discover that the solution requires only a few, noncontroversial steps.

Compellingly readable, Success and Luck shows how a more accurate understanding of the role of chance in life could lead to better, richer, and fairer economies and societies."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
- Luck
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Review
  url: https://www.imf.org/external/pubs/ft/fandd/2016/09/book2.htm
  icon_pack: fas
  icon: pen-fancy
- name: Review
  url: https://doi.org/10.1177%2F0001839217691734
  icon_pack: fas
  icon: pen-fancy
- name: Review
  url: https://blogs.lse.ac.uk/lsereviewofbooks/2016/06/28/book-review-success-and-luck-good-fortune-and-the-myth-of-meritocracy-by-robert-h-frank/
  icon_pack: fas
  icon: pen-fancy

url_pdf: "https://press.princeton.edu/titles/10663.html"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
