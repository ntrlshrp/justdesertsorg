---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Can 'Baby Bonds' Eliminate the Racial Wealth Gap in Putative Post-Racial America?"
authors: 
- Darrick Hamilton
- William Darity Jr.
date: 2010-01-01
doi: "10.1007/s12114-010-9063-1"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-30T16:56:57-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Review of Black Political Economy"
publication_short: ""

abstract: "Despite an enormous and persistent black-white wealth gap, the ascendant American narrative is one that proclaims that our society has transcended the racial divide. The proclamation often is coupled with the claim that remaining disparities are due primarily to dysfunctional behavior on the part of blacks. In such a climate it appears the only acceptable remedial social policies are those that are facially race neutral. However, even without the capacity to redistribute assets directly on the basis of race, our nation still can do so indirectly by judiciously using wealth as the standard for redistributive measures. We offer a bold progressive child development account type program that could go a long way towards eliminating the racial wealth gap."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
- Race
- Childhood Asset Transfers
- Racial Wealth Gap
- Post Racial America
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://www.whiteprivilegeconference.com/pdf/WPC14_baby_bonds.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
