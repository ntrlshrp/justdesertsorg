---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Distributive Justice and Access to Advantage: G. A. Cohen's Egalitarianism"
authors: 
- Alexander Kaufman, ed.
date: 2014-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-03T20:55:43-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Should egalitarians seek to equalize welfare, resources, opportunity, or some other indicator of well-being? G. A. Cohen's classic writings offer one of the most influential responses to the currency of the egalitarianism justice question. In this volume, major scholars assess and respond to Cohen's contribution."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Egalitarianism
- Equality of Opportunity
categories: 
- Economic Justice
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
  - name: Review
    url: https://doi.org/10.1093/pq/pqw046
    icon_pack: fas
    icon: pen-fancy

url_pdf: "https://www.cambridge.org/us/academic/subjects/politics-international-relations/political-theory/distributive-justice-and-access-advantage-g-cohens-egalitarianism"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
