---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Economic Mobility: Is the American Dream Alive and Well?"
authors: 
- Isabel V. Sawhill
- John E. Morton
date: 2007-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-12T15:05:28-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "For more than two centuries, economic opportunity and the prospect of upward mobility have formed the bedrock upon which the American story has been anchored — inspiring people in distant lands to seek our shores and sustaining the unwavering optimism of Americans at home. From the hopes of the earliest settlers to the aspirations of today’s diverse population, the American Dream unites us in a common quest for individual and national success. But new data suggest that this once solid ground may well be shifting. This raises provocative questions about the continuing ability of all Americans to move up the economic ladder and calls into question whether the American economic meritocracy is still alive and well.

Recent studies suggest that there is less economic mobility in the United States than has long been presumed."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Economic Mobility
- Equality of Opportunity
- Poverty
- United States
categories: 
- Economic Justice
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.brookings.edu/wp-content/uploads/2016/06/05useconomics_morton.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.brookings.edu/research/economic-mobility-is-the-american-dream-alive-and-well/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: 
- economic-mobility-project

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
