---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Moral Luck"
authors:
- Dana K. Nelkin
date: 2019-04-19
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T17:00:35-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "The Stanford Encyclopedia of Philosophy"
publication_short: ""

abstract: "Moral luck occurs when an agent can be correctly treated as an object of moral judgment despite the fact that a significant aspect of what she is assessed for depends on factors beyond her control. Bernard Williams writes, “when I first introduced the expression moral luck, I expected to suggest an oxymoron” (Williams 1993, 251). Indeed, immunity from luck has been thought by many to be part of the very essence of morality. And yet, as Williams (1981) and Thomas Nagel (1979) showed in their now classic pair of articles, it appears that our everyday judgments and practices commit us to the existence of moral luck."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Luck
- Moral Responsibility
- Aristotle
- Ethics
- Causation | In the Law
- Moral Character
- Compatibilism
- Determinism | causal
- Free Will
- Incompatibilism
- Nondeterministic Theories of Free Will
- Punishment
- Legal Punishment
- Egalitarianism
categories: 
- Philosophy
- Encyclopedia Article
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project: "https://plato.stanford.edu/index.html"
url_slides:
url_source: "https://plato.stanford.edu/entries/moral-luck/"
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

# Moral Luck

Moral luck occurs when an agent can be correctly treated as an object of moral judgment despite the fact that a significant aspect of what she is assessed for depends on factors beyond her control. Bernard Williams writes, “when I first introduced the expression moral luck, I expected to suggest an oxymoron” (Williams 1993, 251). Indeed, immunity from luck has been thought by many to be part of the very essence of morality. And yet, as Williams (1981) and Thomas Nagel (1979) showed in their now classic pair of articles, it appears that our everyday judgments and practices commit us to the existence of moral luck. The problem of moral luck arises because we seem to be committed to the general principle that we are morally assessable only to the extent that what we are assessed for depends on factors under our control (call this the “Control Principle”). At the same time, when it comes to countless particular cases, we morally assess agents for things that depend on factors that are not in their control. And making the situation still more problematic is the fact that a very natural line of reasoning suggests that it is impossible to morally assess anyone for anything if we adhere to the Control Principle.

##### Table of Contents

1. Generating the Problem of Moral Luck and Kinds of Luck
2. Implications for Other Debates
  2. The Justification of Laws and Punishment
  2. Egalitarianism
3. Kinds of Moral Assessment
4. Responding to the Problem: Three Approaches
  4. Denial
    4. Denying Moral Luck and Preserving the Centrality of Morality
    4. Denying Moral Luck and Setting Aside Morality in Favor of Ethics
  4. Acceptance
    4. Accepting Moral Luck and Revising our Practices
    4. Accepting Moral Luck without Revision
  4. Incoherence
5. Conclusion
- Bibliography
- Academic Tools
- Other Internet Resources
- Related Entries

[Continue Reading](https://plato.stanford.edu/entries/moral-luck/)
