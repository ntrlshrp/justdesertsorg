---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "A Very Uneven Playing Field: Economic Mobility in the United States"
authors: 
- Pablo A. Mitnik
- Victoria Bryant
- David B. Grusky
date: 2018-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-12T16:06:38-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "We present results from a new data set, the Statistics of Income Mobility Panel, that has been assembled from tax and other administrative sources to provide evidence on economic mobility and persistence in the United States. This data set allows us to take on the methodological problems that have complicated previous efforts to estimate intergenerational earnings and income elasticities. We find that the elasticities for women’s income, men’s income, and men’s earnings are as high as all but the highest of the previously reported survey-based estimates. Because the intergenerational curves are especially steep within the parental-income region defined by the 50th to 90th percentiles, approximately two-thirds of the inequality between poor and well-off families is passed on to the next generation. This extreme persistence cannot be attributed to any single factor. Instead, the U.S. is exceptional with respect to virtually all factors governing economic persistence, including the returns to human capital, the amount of public investment in the human capital of low-income children, the amount of socioeconomic segregation, and the progressiveness of the tax-and-transfer system. For each of these four factors, the U.S. has opted for policies that are mobility-reducing, with the implication that any substantial increase in mobility will likely require a wide-ranging package of reforms that cut across many institutions."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Economic Mobility
- Equality of Opportunity
- Administrative Data
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://web.stanford.edu/~pmitnik/Mitnik_Bryant_Grusky_2018_econ_mob_wp.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
