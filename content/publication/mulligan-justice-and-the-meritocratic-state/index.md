---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Justice and the Meritocratic State"
authors: 
- Thomas Mulligan
date: 2018-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-10T13:35:56-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Like American politics, the academic debate over justice is polarized, with almost all theories of justice falling within one of two traditions: egalitarianism and libertarianism. This book provides an alternative to the partisan standoff by focusing not…"

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Desert
- Merit
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
  - name: Review
    url: https://ndpr.nd.edu/news/justice-and-the-meritocratic-state/
    icon_pack: fas
    icon: pen-fancy

url_pdf: https://www.routledge.com/Justice-and-the-Meritocratic-State-1st-Edition/Mulligan/p/book/9781138283800
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: 
- political-philosophy-for-the-real-world

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
