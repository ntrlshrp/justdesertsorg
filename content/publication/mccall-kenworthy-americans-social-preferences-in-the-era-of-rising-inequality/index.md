---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Americans' Social Preferences in the Era of Rising Inequality"
authors: 
- Leslie McCall
- Lane Kenworthy
date: 2009-01-01
doi: "10.1017/S1537592709990818"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T23:04:45-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Perspectives on Politics"
publication_short: ""

abstract: "Rising income inequality has been a defining trend of the past generation, yet we know little about its impact on social policy formation. We evaluate two dominant views about public opinion on rising inequality: that Americans do not care much about inequality of outcomes, and that a rise in inequality will lead to an increase in demand for government redistribution. Using time series data on views about income inequality and social policy preferences in the 1980s and 1990s from the General Social Survey, we find little support for these views. Instead, Americans do tend to object to inequality and increasingly believe government should act to redress it, but not via traditional redistributive programs. We examine several alternative possibilities and provide a broad analytical framework for reinterpreting social policy preferences in the era of rising inequality. Our evidence suggests that Americans may be unsure or uninformed about how to address rising inequality and thus swayed by contemporaneous debates. However, we also find that Americans favor expanding education spending in response to their increasing concerns about inequality. This suggests that equal opportunity may be more germane than income redistribution to our understanding of the politics of inequality."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Values
- Economic Inequality
- United States
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://doi.org/10.1017/S1537592709990818
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
