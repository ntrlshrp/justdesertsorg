---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The Undeserving Rich: American Beliefs about Inequality, Opportunity, and Redistribution"
authors: 
- Leslie McCall
date: 2013-01-01
doi: "10.1017/CBO9781139225687"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T23:12:27-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "It is widely assumed that Americans care little about income inequality, believe opportunities abound, admire the rich, and dislike redistributive policies. Leslie McCall contends that such assumptions are based on both incomplete survey data and economic conditions of the past and not present. In fact, Americans have desired less inequality for decades, and McCall's book explains why. Americans become most concerned about inequality in times of inequitable growth, when they view the rich as prospering while opportunities for good jobs, fair pay and high quality education are restricted for everyone else. As a result, they favor policies to expand opportunity and redistribute earnings in the workplace, reducing inequality in the market rather than redistributing income after the fact with tax and spending policies. This book resolves the paradox of how Americans can express little enthusiasm for welfare state policies and still yearn for a more equitable society, and forwards a new model of preferences about income inequality rooted in labor market opportunities rather than welfare state policies."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Economic Inequality
- United States
- Values
- Survey
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.cambridge.org/core/books/undeserving-rich/F00392D0112132883DFFB034DF5A50FC
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
