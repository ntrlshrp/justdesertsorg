---
title: "The Meritocracy Myth"
date: 2018-01-01
publishDate: 2019-12-04T15:41:47.416509Z
authors: ["Stephen J. McNamee"]
publication_types: ["5"]
abstract: "The Meritocracy Myth challenges the widely held American belief in meritocracy—that people get out of the system what they put into it based on individual merit. The book examines talent, attitude, work ethic, and character as elements of merit and evaluates the effect of nonmerit factors such as family background, social connections, luck, market conditions, unequal educational opportunities, and discrimination.  The fourth edition has been revised and streamlined throughout. It features new material on the current economic and political climate; the reasons behind the increasing levels of inequality in the United States and globally; how economic, social, and cultural factors shaped Donald Trump’s rise to political prominence, and more. The fourth edition includes a new chapter on marriage and mobility that examines how patterns in marriage tend to increase the concentration of wealth and pass on nonmerit advantages to children, furthering trends toward social inequality.  A compelling book on an often-overlooked topic, The Meritocracy Myth is ideal for introducing students to this provocative topic while sparking discussion and reflection."
featured: false
url_source: "https://rowman.com/isbn/9781538103395/the-meritocracy-myth-fourth-edition"
---

