---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Genes as Tags: The Tax Implications of Widely Available Genetic Information"
authors: 
- Kyle Logue
- Joel Slemrod
date: 2008-01-01
doi: "10.17310/ntj.2008.4S.04"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-20T20:46:37-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "National Tax Journal"
publication_short: ""

abstract: "Advances in genetic research promise to loosen the tradeoff between progressivity and efficiency by allowing tax liability (or transfer eligibility) to be based in part on immutable characteristics of individuals (“tags”) that are correlated with their expected lot in life. Use of genetic tags would reduce reliance on tax bases (such as income) that are subject to individual choices and, therefore, subject to inefficient distortion to those choices. If genetic information can be used by private employers and insurers, the case for basing tax in part on it becomes more compelling, as genetic inequalities would be exacerbated by market forces."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Endowment Tax
- Natural Lottery
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.ntanet.org/NTJ/61/4/ntj-v61n04p843-63-genes-tags-tax-implications.pdf?v=%CE%B1&r=7223357900225971
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.ntanet.org/NTJ/61/4/ntj-v61n04p843-63-genes-tags-tax-implications.html
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
