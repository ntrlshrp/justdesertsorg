---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Taxing Endowment"
authors: 
- Lawrence Zelenak
date: 2006-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-20T20:23:21-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Duke Law Journal"
publication_short: ""

abstract: "Writing in 1888, Francis A. Walker, the first president of the American Economic Association, claimed that the ideal tax base was neither wealth, nor income, nor consumption, but rather 'faculty, or native or acquired power of production.' According to Walker, only this tax base—what people could earn, rather than the often lesser amounts they actually earn—could achieve an equitable distribution of the tax burden. Although he conceded that practical difficulties made adoption of a pristine faculty tax impossible, he urged that it should nevertheless be 'held in view, as furnishing the line from which to measure all departures from the equities of contribution, as one or another form of taxation . . . comes to be adopted for meeting the wants of government.'

The aim of this review Essay is to provide a reasonably comprehensive critical guide to the state of the endowment tax discussion. Because the utilitarian arguments for and against endowment taxation are quite different from the liberal egalitarian arguments, this Essay considers the merits of endowment taxation from both perspectives."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Endowment Tax
- Tax Policy
- Liberalism
- Utilitarianism
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://scholarship.law.duke.edu/cgi/viewcontent.cgi?article=1290&context=dlj
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://scholarship.law.duke.edu/dlj/vol55/iss6/2/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
