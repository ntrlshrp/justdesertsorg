---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Commencement Address at Howard University"
authors: 
- President Lyndon B. Johnson
date: 1965-06-04
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-19T23:26:39-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Freedom is the right to share, share fully and equally, in American society—to vote, to hold a job, to enter a public place, to go to school. It is the right to be treated in every part of our national life as a person equal in dignity and promise to all others.

But freedom is not enough. You do not wipe away the scars of centuries by saying: Now you are free to go where you want, and do as you desire, and choose the leaders you please."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- United States
- Racial Inequality
- Equality of Opportunity
- Leveling the Playing Field
categories: 
- Oration
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://thf_media.s3.amazonaws.com/2011/pdf/FP_PS30.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: http://www.heritage.org/initiatives/first-principles/primary-sources/from-opportunity-to-outcomes-lbj-expands-the-meaning-of-equality
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
