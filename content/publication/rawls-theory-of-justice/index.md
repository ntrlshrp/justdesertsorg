---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "A Theory of Justice"
authors: 
- John Rawls
date: 1971-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-06T19:01:21-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Since it appeared in 1971, John Rawls’s A Theory of Justice has become a classic. The author has now revised the original edition to clear up a number of difficulties he and others have found in the original book.

Rawls aims to express an essential part of the common core of the democratic tradition—justice as fairness—and to provide an alternative to utilitarianism, which had dominated the Anglo-Saxon tradition of political thought since the nineteenth century. Rawls substitutes the ideal of the social contract as a more satisfactory account of the basic rights and liberties of citizens as free and equal persons. “Each person,” writes Rawls, “possesses an inviolability founded on justice that even the welfare of society as a whole cannot override.” Advancing the ideas of Rousseau, Kant, Emerson, and Lincoln, Rawls’s theory is as powerful today as it was when first published."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: 
- Political Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Review
  url: https://doi.org/10.2307/1959089
  icon_pack: fas
  icon: pen-fancy

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.hup.harvard.edu/catalog.php?isbn=9780674000780
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
