---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Equality of Opportunity"
authors: 
- John E. Roemer
date: 1998-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-03T20:55:16-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "John Roemer points out that there are two views of equality of opportunity that are widely held today. The first, which he calls the nondiscrimination principle, states that in the competition for positions in society, individuals should be judged only on attributes relevant to the performance of the duties of the position in question. Attributes such as race or sex should not be taken into account. The second states that society should do what it can to level the playing field among persons who compete for positions, especially during their formative years, so that all those who have the relevant potential attributes can be considered.

Common to both positions is that at some point the principle of equal opportunity holds individuals accountable for achievements of particular objectives, whether they be education, employment, health, or income. Roemer argues that there is consequently a “before” and an “after” in the notion of equality of opportunity: before the competition starts, opportunities must be equalized, by social intervention if need be; but after it begins, individuals are on their own. The different views of equal opportunity should be judged according to where they place the starting gate which separates “before” from “after.” Roemer works out in a precise way how to determine the location of the starting gate in the different views."

# Summary. An optional shortened abstract.
summary: "Roemer argues that there is consequently a “before” and an “after” in the notion of equality of opportunity: before the competition starts, opportunities must be equalized, by social intervention if need be; but after it begins, individuals are on their own. The different views of equal opportunity should be judged according to where they place the starting gate which separates “before” from “after.” Roemer works out in a precise way how to determine the location of the starting gate in the different views."

tags: 
- Economics
- Equality of Opportunity
categories: 
- Economic Justice
- Economics | General
- Ethics and Moral Philosophy
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
  - name: Review
    url: https://doi.org/10.1086/338614
    icon_pack: fas
    icon: pen-fancy

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: "http://www.hup.harvard.edu/catalog.php?isbn=9780674004221"
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
