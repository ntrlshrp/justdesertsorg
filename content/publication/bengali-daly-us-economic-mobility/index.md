---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "U.S. Economic Mobility: The Dream and the Data"
authors: 
- Leila Bengali
- Mary Daly
date: 2013-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-12T15:31:43-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Economic mobility is a core principle of the American narrative and the basis for the American Dream. However, research suggests that the United States may not be as mobile as Americans believe. The United States has high absolute mobility in the sense that children readily become richer than their parents. But the nation appears to fall short on relative mobility, which is the ability of children to change their rank in the income distribution relative to their parents."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Economic Mobility
- Equality of Opportunity
- United States
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.frbsf.org/economic-research/files/el2013-06.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.frbsf.org/economic-research/publications/economic-letter/2013/march/us-economic-mobility-dream-data/
url_video: http://www.frbsf.org/education/teachers/economics-in-person/economic-mobility-in-the-united-states.html

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
