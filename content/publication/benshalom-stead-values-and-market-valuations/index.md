---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Values and (Market) Valuations: A Critique of the Endowment Tax Consensus"
authors: 
- Ilan Benshalom
- Kendra Stead
date: 2010-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-09-09T15:59:44-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Northwestern University Law Review"
publication_short: ""

abstract: "A consensus is hard to come by, and to the extent you find one you should be suspicious of it. There are hints of such a consensus among several prominent tax scholars—endorsement of endowment as the ideal tax base. An endowment tax would be based on individuals’ ability to earn income rather than on income actually earned. This Article challenges this agenda at a crucial moment, as developments in genetics and quantitative social sciences may start allowing endowment taxation to creep outside the boundaries of abstract tax theory, potentially affecting real tax policy arrangements."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Endowment Tax
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.law.northwestern.edu/lawreview/v104/n4/1511/LR104n4Benshalom&Stead.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2192269
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
