---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Luck Egalitarianism - A Primer"
authors: 
- Richard J. Arneson
date: 2011-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-10T01:27:03-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "Responsibility and Distributive Justice"
publication_short: ""

abstract: "This essay surveys varieties of the luck egalitarian project in an exploratory spirit, seeking to identify lines of thought that are worth developing further and that might ultimately prove morally acceptable. I do not attend directly to the critics and assess their concerns; I have done that in other essays. 7 I do seek to identify some large fault lines, divisions in ways of approaching the task of constructing a theory of justice or of conceiving its substance. These are controversial in the sense that in the present state of discussion it is unclear how best to view them or to which side it is better to scramble. But in the end of course I’m not a moral geographer and map-maker, just an involved spectator/tourist offering yet another view of the cathedral."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
- Luck-Egalitarianism
categories: 
- Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://philosophyfaculty.ucsd.edu/faculty/rarneson/LUCKEGALOUP%20-%204.%20Arneson.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: publication/knight-stemplowska-responsibility-and-distributive-justice
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
