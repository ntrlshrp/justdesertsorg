---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "New Estimates of Intergenerational Mobility Using Administrative Data"
authors: 
- Pablo A. Mitnik
- Victoria Bryant
- Michael Weber
- David B. Grusky
date: 2015-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-12T15:49:46-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "This report presents analyses of a new data set, the Statistics of Income Mobility Panel, that has been assembled to provide new evidence on economic mobility and the implications of tax policy for economic mobility. Because the data are of unusually high quality, they provide the foundation for a comprehensive report on intergenerational income and earnings mobility in the United States. We describe here (a) the rationale for measuring economic mobility and for using intergenerational income and earnings elasticities to do so; (b) the current state of the evidence on the size of intergenerational elasticities; (c) the rationale for assembling the Statistics of Income Mobility (SOI-M) Panel and the key features of this new panel; (d) the main reasons why it’s necessary to abandon some of the field’s long-standing methodological conventions in favor of a new approach for specifying and estimating intergenerational elasticities; (e) the new estimates of average intergenerational elasticities for individual earnings and for family income; (f) the extent to which intergenerational persistence varies across regions of the parental income distribution; (g) the extent of intergenerational persistence among families that are very far apart in the income distribution; (h) the robustness of our estimates to the treatment of nonfilers; (i) the extent to which estimates of after-tax elasticities are consistent with estimates of pre-tax elasticities; (j) the effects of low-income tax credits on economic persistence; and (k) the role of gender and marriage in generating total-income elasticities."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Economic Mobility
- Administrative Data
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
- name: Media
  url: post/pinsker-america-is-even-less-socially-mobile-than-most-economists-thought/
  icon_pack: fas
  icon: pen

url_pdf: https://www.irs.gov/pub/irs-soi/15rpintergenmobility.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
