---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Luck"
authors: 
- Fernando Broncano-Berrocal
date: 2016-03-26
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T16:48:29-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Internet Encyclopedia of Philosophy"
publication_short: ""

abstract: "Winning a lottery, being hit by a stray bullet, or surviving a plane crash, all are instances of a mundane phenomenon: luck. Mundane as it is, the concept of luck nonetheless plays a pivotal role in central areas of philosophy, either because it is the key element of widespread philosophical theses or because it gives rise to challenging puzzles."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Luck
categories: 
- Philosophy
- Encyclopedia Article
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project: https://www.iep.utm.edu/
url_slides:
url_source: "https://www.iep.utm.edu/luck/"
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

# Luck

Winning a lottery, being hit by a stray bullet, or surviving a plane crash, all are instances of a mundane phenomenon: luck. Mundane as it is, the concept of luck nonetheless plays a pivotal role in central areas of philosophy, either because it is the key element of widespread philosophical theses or because it gives rise to challenging puzzles. For example, a common claim in philosophy of action is that acting because of luck prevents free action. A platitude in epistemology is that coming to believe the truth by sheer luck is incompatible with knowing. If two people act in the same way but the consequences of one of their actions are worse due to luck, should we morally assess them in the same way? Is the inequality of a person unjust when it is caused by bad luck? These two complex issues are a matter of controversy in ethics and political philosophy, respectively.

A legitimate question is whether the concept of luck itself is worthy of philosophical investigation. One might think that it is not given (i) how acquainted we are with the phenomenon of luck in everyday life and (ii) the fact that progress has been made in the aforementioned debates on the assumption of a pre-theoretical understanding of the notion.

However, the idea that a rigorous analysis of the general concept of luck might serve to make further progress in areas of philosophy where the notion plays a fundamental role has motivated a recent and growing philosophical literature on the nature of luck itself. Although some might be skeptical that investigating the nature of luck in general can help shed some light on long-standing philosophical debates such as the nature of knowledge—see Ballantyne 2014—it is hardly sustainable that no general account of luck will be able to ground any substantive claim in areas of philosophy where the notion is constantly invoked but left undefined. This article gives an overview of current philosophical theorizing about the concept of luck itself.

##### Table of Contents

1. Preliminary Remarks
  1. The Bearers of Luck
  1. The Target of the Analysis
  1. General Features of Luck
1. Luck and Significance
1. Probabilistic Accounts
  1. Objective Accounts
  1. Subjective Accounts
1. Modal Accounts
1. Lack of Control Accounts
1. Hybrid Accounts
1. Luck and Related Concepts
  1. Accidents
  1. Coincidences
  1. Fortune
  1. Risk
  1. Indeterminacy
1. References and Further Reading

[Continue Reading](https://www.iep.utm.edu/luck/)
