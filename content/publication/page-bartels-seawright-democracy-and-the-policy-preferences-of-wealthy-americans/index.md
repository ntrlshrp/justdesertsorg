---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Democracy and the Policy Preferences of Wealthy Americans"
authors: 
- Benjamin I. Page
- Larry M. Bartels
- Jason Seawright
date: 2013-01-01
doi: "10.1017/S153759271200360X"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T22:56:14-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Perspectives on Politics"
publication_short: ""

abstract: "It is important to know what wealthy Americans seek from politics and how (if at all) their policy preferences differ from those of other citizens. There can be little doubt that the wealthy exert more political influence than the less affluent do. If they tend to get their way in some areas of public policy, and if they have policy preferences that differ significantly from those of most Americans, the results could be troubling for democratic policy making. Recent evidence indicates that “affluent” Americans in the top fifth of the income distribution are socially more liberal but economically more conservative than others. But until now there has been little systematic evidence about the truly wealthy, such as the top 1 percent. We report the results of a pilot study of the political views and activities of the top 1 percent or so of US wealth-holders. We find that they are extremely active politically and that they are much more conservative than the American public as a whole with respect to important policies concerning taxation, economic regulation, and especially social welfare programs. Variation within this wealthy group suggests that the top one-tenth of 1 percent of wealth-holders (people with $40 million or more in net worth) may tend to hold still more conservative views that are even more distinct from those of the general public. We suggest that these distinctive policy preferences may help account for why certain public policies in the United States appear to deviate from what the majority of US citizens wants the government to do. If this is so, it raises serious issues for democratic theory."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Economic Inequality
- Values
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://doi.org/10.1017/S153759271200360X
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
