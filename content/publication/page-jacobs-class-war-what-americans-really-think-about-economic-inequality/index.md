---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Class War? What Americans Really Think About Economic Inequality"
authors: 
- Benjamin I. Page
- Lawrence R. Jacobs
date: 2009-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T18:38:41-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Recent battles in Washington over how to fix America’s fiscal failures strengthened the widespread impression that economic issues sharply divide average citizens. Indeed, many commentators split Americans into two opposing groups: uncompromising supporters of unfettered free markets and advocates for government solutions to economic problems. But such dichotomies, Benjamin Page and Lawrence Jacobs contend, ring false. In Class War? they present compelling evidence that most Americans favor free enterprise and practical government programs to distribute wealth more equitably.

At every income level and in both major political parties, majorities embrace conservative egalitarianism—a philosophy that prizes individualism and self-reliance as well as public intervention to help Americans pursue these ideals on a level playing field. Drawing on hundreds of opinion studies spanning more than seventy years, including a new comprehensive survey, Page and Jacobs reveal that this worldview translates to broad support for policies aimed at narrowing the gap between rich and poor and creating genuine opportunity for all. They find, for example, that across economic, geographical, and ideological lines, most Americans support higher minimum wages, improved public education, wider access to universal health insurance coverage, and the use of tax dollars to fund these programs.

In this surprising and heartening assessment, Page and Jacobs provide our new administration with a popular mandate to combat the economic inequity that plagues our nation."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Economic Inequality
- Survey
categories: 
- American Government and Politics
- Political Behavior and Public Opinion
- Social Change
- Social Movements
- Political Sociology 
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.press.uchicago.edu/ucp/books/book/chicago/C/bo6683623.html
url_code:
url_dataset: https://www.press.uchicago.edu/books/page/index.html
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
