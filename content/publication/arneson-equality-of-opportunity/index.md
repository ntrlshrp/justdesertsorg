---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Equality of Opportunity"
authors: 
- Richard Arneson
date: 2015-03-25
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T17:18:23-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "The Stanford Encyclopedia of Philosophy"
publication_short: ""

abstract: "Equality of opportunity is a political ideal that is opposed to caste hierarchy but not to hierarchy per se. When equality of opportunity prevails, the assignment of individuals to places in the social hierarchy is determined by some form of competitive process, and all members of society are eligible to compete on equal terms. Different conceptions of equality of opportunity construe this idea of competing on equal terms variously."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Affirmative Action
- Equality
- Distributive Justice
- Luck
- Justice and Bad Luck
- Egalitarianism
categories: 
- Encyclopedia Article
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project: https://plato.stanford.edu/index.html
url_slides:
url_source: https://plato.stanford.edu/entries/equal-opportunity/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---



Equality of opportunity is a political ideal that is opposed to caste hierarchy but not to hierarchy per se. The background assumption is that a society contains a hierarchy of more and less desirable, superior and inferior positions. Or there may be several such hierarchies. In a caste society, the assignment of individuals to places in the social hierarchy is fixed by birth. The child acquires the social status of his or her parents at least if their union is socially sanctioned. Social mobility may be possible in a caste society, but the process whereby one is admitted to a different level of the hierarchy is open only to some individuals depending on their initial ascriptive social status. In contrast, when equality of opportunity prevails, the assignment of individuals to places in the social hierarchy is determined by some form of competitive process, and all members of society are eligible to compete on equal terms. Different conceptions of equality of opportunity construe this idea of competing on equal terms variously.

##### Table of Contents

1. Formal Equality of Opportunity
  1. Being qualified for a competitive position.
  1. Formal Equality of Opportunity and Market Freedom
  1. Discrimination and Formal Equality of Opportunity
  1. Discrimination and Derogation
2. Substantive Equality of Opportunity
  2. Discrimination and Fair Equality of Opportunity
  2. Democratic Equality of Fair Opportunity
  2. Other Conceptions of Substantive Equality of Opportunity
  2. Affirmative Action
3. Social Mobility and Equality of Opportunity
4. The Scope of Equality of Opportunity
5. Widening the Equal Opportunity Ideals
6. Legal Enforcement of Equality of Opportunity
  6. Antidiscrimination Law: Disparate Treatment and Disparate Impact
7. The Level Playing Field Conception: Luck Egalitarianism
8. A Kantian Interpretation of Equality of Opportunity
9. Equality of Opportunity and Meritocracy
10. Justifications of Equality of Opportunity
11. Critiques
  11. The Libertarian Critique: Robert Nozick's Version.
  11. The Libertarian Critique: Richard Epstein's Version
  11. Genetics and the Relevance of Equality of Opportunity
  11. The Morality of Inclusion
  11. Relational Equality
  11. The Leveling-down Objection
  11. The Fairness-to-the-Worse-Off Critique of FEO
12. Conclusion
- Bibliography
- Academic Tools
- Other Internet Resources
- Related Entries

[Continue Reading](https://plato.stanford.edu/entries/equal-opportunity/)
