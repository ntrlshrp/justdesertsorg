---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Luck Egalitarianism: Equality, Responsibility, and Justice"
authors: 
- Carl Knight
date: 2009-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-20T19:57:10-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "How should we decide which inequalities between people are justified, and which are unjustified? One answer is that such inequalities are only justified where there is a corresponding variation in responsible action or choice on the part of the persons concerned. This view, which has become known as 'luck egalitarianism', has come to occupy a central place in recent debates about distributive justice. This book is the first full length treatment of this significant development in contemporary political philosophy. Each of its three parts addresses a key question concerning the theory. Which version of luck egalitarian comes closest to realizing luck egalitarian objectives? Does luck egalitarianism succeed as a view of egalitarian justice? And is it sound as an account of distributive justice in general? The book provides a distinctive answer to each of these questions, along the way engaging with the leading theorists identified in the literature as luck egalitarians, such as Richard Arneson, G. A. Cohen, and Ronald Dworkin, as well as the most influential critics, including Elizabeth Anderson, Marc Fleurbaey, Susan Hurley, Samuel Scheffler, and Jonathan Wolff."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Luck-Egalitarianism
- Equality of Opportunity
- Responsibility
categories: 
- Political Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Review
  url: https://doi.org/10.1093/analys/anq055
  icon_pack: fas
  icon: pen-fancy
- name: Review
  url: https://doi.org/10.1163/174552412X619111
  icon_pack: fas
  icon: pen-fancy

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.worldcat.org/title/luck-egalitarianism-equality-responsibility-and-justice/oclc/286520566
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
