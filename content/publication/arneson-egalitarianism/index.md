---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Egalitarianism"
authors: 
- Richard J. Arneson
date: 2013-04-24
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-10T00:09:01-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "The Stanford Encyclopedia of Philosophy"
publication_short: ""

abstract: "Egalitarianism is a trend of thought in political philosophy. An egalitarian favors equality of some sort: People should get the same, or be treated the same, or be treated as equals, in some respect. An alternative view expands on this last-mentioned option: People should be treated as equals, should treat one another as equals, should relate as equals, or enjoy an equality of social status of some sort. Egalitarian doctrines tend to rest on a background idea that all human persons are equal in fundamental worth or moral status. So far as the Western European and Anglo-American philosophical tradition is concerned, one significant source of this thought is the Christian notion that God loves all human souls equally. Egalitarianism is a protean doctrine, because there are several different types of equality, or ways in which people might be treated the same, or might relate as equals, that might be thought desirable. In modern democratic societies, the term “egalitarian” is often used to refer to a position that favors, for any of a wide array of reasons, a greater degree of equality of income and wealth across persons than currently exists."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Egalitarianism
- Affirmative Action
- Consequentialism
- Desert
- Equality
- Equality of Opportunity
- Distributive Justice
- Luck
- Justice and Bad Luck
categories: 
- Philosophy
- Encyclopedia Article
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project: https://plato.stanford.edu/index.html
url_slides:
url_source: https://plato.stanford.edu/entries/egalitarianism/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
