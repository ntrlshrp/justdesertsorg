---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Theories of Distributive Justice and Limitations on Taxation: What Rawls Demands from Tax Systems"
authors: 
- Linda Sugin
date: 2004-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-09-12T00:20:55-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Fordham Law Review"
publication_short: ""

abstract: "Murphy and Nagel's critique prevents endorsement of specific tax policies in the name of justice, but invites a different, narrower inquiry: whether a particular tax system (or even tax provision) precludes a just economic arrangement in society. For example, if the tax system consisted of confiscating all the property of the poorest 20% of the population, no set of remaining government policies could provide an overall economic distribution that liberal egalitarians would consider just. Similarly, the injustice in a tax system that required only black people to pay income taxes could not be cured by any other combination of government institutions. This Essay attempts to map out how such an inquiry would be conducted in light of Rawls"

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Tax Policy
- Rawls
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://ir.lawnet.fordham.edu/cgi/viewcontent.cgi?article=3981&context=flr
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://ir.lawnet.fordham.edu/flr/vol72/iss5/27/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
