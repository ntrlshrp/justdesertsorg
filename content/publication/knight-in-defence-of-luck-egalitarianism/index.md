---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "In Defence of Luck Egalitarianism"
authors: 
- Carl Knight
date: 2005-01-01
doi: "10.1007/s11158-004-4973-z"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-20T00:13:55-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Res Publica"
publication_short: ""

abstract: "This paper considers issues raised by Elizabeth Anderson’s recent critique of the position she terms ‘luck egalitarianism’. It is maintained that luck egalitarianism, once clarified and elaborated in certain regards, remains the strongest egalitarian stance. Anderson’s arguments that luck egalitarians abandon both the negligent and prudent dependent caretakers fails to account for the moderate positions open to luck egalitarians and overemphasizes their commitment to unregulated market choices. The claim that luck egalitarianism insults citizens by redistributing on the grounds of paternalistic beliefs, pity and envy, and by making intrusive and stigmatizing judgments of responsibility, fails accurately to characterize the luck egalitarian’s rationale for redistribution and relies upon luck egalitarians being insensitive to the danger of stigmatization (which they need not be). The luck egalitarian position is reinforced by the fact that Anderson’s favoured conception of equality, ‘democratic equality’, is counterintuitively indifferent to all unchosen inequalities, including intergenerational inequalities, once bare social minima are met."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Distributive Justice
- Elizabeth Anderson
- Equality
- Luck-Egalitarianism
- Option Luck
- Paternalism
- Pity
- Responsibility
- Ronald Dworkin
- Social Insurance 
categories: 
- Ethics and Moral Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://doi.org/10.1007/s11158-004-4973-z
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
