---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Luck Egalitarianism"
authors: 
- Carl Knight
date: 2013-01-01
doi: "10.1111/phc3.12077"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-19T23:54:10-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Philosophy Compass"
publication_short: ""

abstract: "Luck egalitarianism is a family of egalitarian theories of distributive justice that aim to counteract the distributive effects of luck. This article explains luck egalitarianism's main ideas, and the debates that have accompanied its rise to prominence. There are two main parts to the discussion. The first part sets out three key moves in the influential early statements of Dworkin, Arneson, and Cohen: the brute luck/option luck distinction, the specification of brute luck in everyday or theoretical terms and the specification of advantage as resources, welfare, or some combination of these. The second part covers three later developments: the democratic egalitarian critique of luck egalitarianism, the luck egalitarian acceptance of pluralism, and luck egalitarian doubts about the significance of the brute luck/option luck distinction."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Luck-Egalitarianism
- Distributive Justice
categories: 
- Ethics and Moral Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://doi.org/10.1111/phc3.12077
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
