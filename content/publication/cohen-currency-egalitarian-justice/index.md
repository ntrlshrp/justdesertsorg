---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "On the Currency of Egalitarian Justice"
authors: 
- G. A. Cohen
date: 1989-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-02T00:17:15-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Ethics"
publication_short: ""

abstract: "In his Tanner Lecture of 1979 called 'Equality of What?' Amartya Sen asked what metric egalitarians should use to establish the extent to which their ideal is realized in a given society. What aspect(s) of a person's condition should count in a fundamental way for egalitarians, and not merely as cause of or evidence of or proxy for what they regard as fundamental?"

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: "https://www.mit.edu/~shaslang/mprg/GACohenCEJ.pdf"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
