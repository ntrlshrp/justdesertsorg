---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Income Inequality in the United States, 1913 1998"
authors: 
- Emmanuel Saez
- Thomas Piketty
date: 2003-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-09T23:43:06-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Quarterly Journal of Economics"
publication_short: ""

abstract: "This paper presents new homogeneous series on top shares of income and wages from 1913 to 1998 in the United States using individual tax returns data. Top income and wages shares display a U-shaped pattern over the century. Our series suggest that the large shocks that capital owners experienced during the Great Depression and World War II have had a permanent effect on top capital incomes. We argue that steep progressive income and estate taxation may have prevented large fortunes from fully recovering from these shocks. Top wage shares were at before World War II, dropped precipitously during the war, and did not start to recover before the late 1960s but are now higher than before World War II. As a result, the working rich have replaced the rentiers at the top of the income distribution."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- United States
- Income Distribution
- Tax Policy
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://eml.berkeley.edu//~saez/pikettyqje.pdf
url_code:
url_dataset: http://elsa.berkeley.edu/~saez/#income
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
