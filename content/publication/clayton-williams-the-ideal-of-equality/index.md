---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The Ideal of Equality"
authors: 
- Matthew Clayton
- Andrew Williams
date: 2000-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-19T23:42:44-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "One of the central debates within contemporary Anglo-American political philosophy concerns how to formulate an egalitarian theory of distributive justice which gives coherent expression to egalitarian convictions and withstands the most powerful anti-egalitarian objections. This book brings together many of the key contributions to that debate by some of the world's leading political philosophers: Richard Arneson, G.A. Cohen, Ronald Dworkin, Thomas Nagel, Derek Parfit, John Rawls, T.M. Scanlon, and Larry Temkin."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Egalitarianism
- Distributive Justice
categories: 
- Political Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Review
  url: https://mises.org/library/ideal-equality-matthew-clayton-and-andrew-williams
  icon_pack: fas
  icon: pen-fancy

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
