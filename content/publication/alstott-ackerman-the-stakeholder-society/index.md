---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The Stakeholder Society"
authors: 
- Anne Alstott
- Bruce A. Ackerman
date: 1999-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-30T17:16:27-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "A quarter century of trickle-down economics has failed. Economic inequality in the United States has dramatically increased. Many, alas, seem resigned to this growing chasm between rich and poor. But what would happen, ask Bruce Ackerman and Anne Alstott, if America were to make good on its promise of equal opportunity by granting every qualifying young adult a citizen’s stake of eighty thousand dollars? Ackerman and Alstott argue that every American citizen has the right to share in the wealth accumulated by preceding generations. The distribution of wealth is currently so skewed that the stakeholding fund could be financed by an annual tax of two percent on the property owned by the richest forty percent of Americans.

Ackerman and Alstott analyze their initiative from moral, political, economic, legal, and human perspectives. By summoning the political will to initiate stakeholding, they argue, we can achieve a society that is more democratic, productive, and free. Their simple but realistic plan would enhance each young adultís real ability to shape his or her own future. It is, in short, an idea that should be taken seriously by anyone concerned with citizenship, welfare dependency, or social justice in America today."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
- Childhood Asset Transfers
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
  - name: Review
    url: https://doi.org/10.1177%2F03058298990280030413
    icon_pack: fas
    icon: pen-fancy

url_pdf: "https://yalebooks.yale.edu/book/9780300078268/stakeholder-society"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
