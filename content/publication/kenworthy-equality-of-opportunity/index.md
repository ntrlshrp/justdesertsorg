---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Equality of Opportunity"
authors: 
- Lane Kenworthy
date: 2019-04-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-05T00:23:18-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Americans believe in equal opportunity. Surveys consistently find 90% of the public agreeing that “our society should do what is necessary to make sure that everyone has an equal opportunity to succeed,” as figure 1 shows. This level of support is rare.1 It suggests policy makers ought to put equality of opportunity at or near the top of the list of goals they pursue."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
- United States
- Values
- Survey
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://lanekenworthy.net/equality-of-opportunity/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: 
- the-good-society

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

Americans believe in equal opportunity. Surveys consistently find 90% of the public agreeing that “our society should do what is necessary to make sure that everyone has an equal opportunity to succeed,” as figure 1 shows. This level of support is rare.1 It suggests policy makers ought to put equality of opportunity at or near the top of the list of goals they pursue.

Yet true equality of opportunity is unattainable. Equal opportunity requires that each person has equivalent skills, abilities, knowledge, and noncognitive traits upon reaching adulthood, and that’s impossible to achieve. Our capabilities are shaped by genetics, developments in utero, parents, siblings, peers, teachers, preachers, sports coaches, tutors, neighborhoods, and a slew of chance events and occurrences. Society can’t fully equalize, offset, or compensate for these influences.

In fact, if we think about it carefully, few of us truly want equal opportunity, as it would require massive intervention in home life and probably also genetic engineering. Moreover, if parents knew everyone would end up with the same skills and abilities at the end of childhood, they would have little incentive to invest effort and money in their children’s development, and that would result in a lower absolute level of capabilities for everyone.

What we really want is for each person to have the most opportunity possible. We should aim, in Amartya Sen’s helpful formulation, to maximize people’s capability to choose, act, and accomplish.2 Pursuing that goal requires providing greater-than-average help to those with less advantageous circumstances or conditions. That, in turn, would move us closer to equal opportunity, even if, as I’ve just explained, full equality of opportunity is not attainable.

[Continue Reading](https://lanekenworthy.net/equality-of-opportunity/)
