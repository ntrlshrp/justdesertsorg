---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Two Objections to Luck Egalitarianism"
authors: 
- Gerald Lang
date: 2005-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-12T18:22:36-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "The doctrine of ‘Luck Egalitarianism’ can be defined as follows: LUCK EGALITARIANISM A person should not be worse off than anyone else, in respect of some given metric or currency of goods, as a result of brute bad luck. Luck Egalitarianism is a position, or a family of positions, associated principally with the writings of, among others, Ronald Dworkin, G. A. Cohen, Richard Arneson, and John Roemer. It has proved to be a very influential doctrine in recent political philosophy.

The argument unfolds as follows. In section I, and putting to one side various intramural debates amongst proponents of it, I will very briefly sketch the essential components of Luck Egalitarianism. In the remaining part of this article, I consider two objections to Luck Egalitarianism. The first of them is the ‘Egalitarian Fallacy’, and derives from work by Susan Hurley. It is outlined in section II. It is argued, in the same section, that this objection fails. The second objection, the ‘See-Saw Objection’, which builds on Hurley’s work in many respects, is more effective. The See-Saw Objection is presented in section III, and defended against three objections to it in sections IV to VI. In section VII, I distinguish between two forms of bad brute luck, ‘interpersonal’ bad luck, and ‘intrapersonal’ bad luck, and argue that the latter, but not the former, is relevant to the fate of Luck Egalitarianism. The concluding section, section VIII, assesses the level of damage inflicted on Luck Egalitarianism by the See-Saw Objection, and briefly plots some future trajectories for those who are in basic sympathy with the Luck Egalitarian project."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Luck-Egalitarianism
categories: 
- Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://www.trinitinture.com/documents/lang.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
