---
title: "Inequality of Opportunity, Inequality of Income and Economic Growth"
date: 2019-01-01
publishDate: 2019-12-04T02:52:38.246920Z
authors: ["Shekhar Aiyar", "Christian H. Ebeke"]
publication_types: ["3"]
abstract: "We posit that the relationship between income inequality and economic growth is mediated by the level of equality of opportunity, which we identify with intergenerational mobility. In economies characterized by intergenerational rigidities, an increase in income inequality has persistent effects—for example by hindering human capital accumulation— thereby retarding future growth disproportionately. We use several recently developed internationally comparable measures of intergenerational mobility to confirm that the negative impact of income inequality on growth is higher the lower is intergenerational mobility. Our results suggest that omitting intergenerational mobility leads to misspecification, shedding light on why the empirical literature on income inequality and growth has been so inconclusive."
featured: false
publication: ""
url_source: "https://www.imf.org/en/Publications/WP/Issues/2019/02/15/Inequality-of-Opportunity-Inequality-of-Income-and-Economic-Growth-46566"
url_pdf: "https://www.imf.org/~/media/Files/Publications/WP/2019/WPIEA2019034.ashx"

tags:
- Intergenerational Mobility
- Income Inequality
- Growth
- Equality of Opportunity
- Inequality

links:
- name: Media
  url: post/aiyar-ebeke-the-threat-of-inequality-of-opportunity
  icon_pack: fas
  icon: pen
---

