---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Meaningful Differences in the Everyday Experience of Young American Children"
authors: 
- Betty Hart
- Todd R. Risley
date: 1995-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-19T20:04:33-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "This study of ordinary families and how they talk to their very young children is no ordinary study at all. Betty Hart and Todd Risley wanted to know why, despite best efforts in preschool programs to equalize opportunity, children from low-income homes remain well behind their more economically advantaged peers years later in school. Their painstaking study began by recording each month---for 2-1/2 years---one full hour of every word spoken at home between parent and child in 42 families, categorized as professional, working class, or welfare families. Years of coding and analyzing every utterance in 1,318 transcripts followed. Rare is a database of this quality. 'Remarkable,' says Assistant Secretary of Education Grover (Russ) Whitehurst, of the findings: By age 3, the recorded spoken vocabularies of the children from the professional families were larger than those of the parents in the welfare families. Between professional and welfare parents, there was a difference of almost 300 words spoken per hour. Extrapolating this verbal interaction to a year, a child in a professional family would hear 11 million words while a child in a welfare family would hear just 3 million. The implications for society are staggering: Hart and Risley's follow-up studies at age 9 show that the large differences in the amount of children's language experience were tightly linked to large differences in child outcomes. And yet the implications are encouraging, too. As the authors conclude their preface to the 2002 printing of Meaningful Differences, 'the most important aspect to evaluate in child care settings for very young children is the amount of talk actually going on, moment by moment, between children and their caregivers.' By giving children positive interactions and experiences with adults who take the time to teach vocabulary, oral language concepts, and emergent literacy concepts, children should have a better chance to succeed at school and in the workplace. Learn more about how parent and children's language interactions affect learning to talk in Hart & Risley's companion book The Social World of Children Learning to Talk."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Children
- Inequality
- Social Science
- Sociology | General
- Family and Relationships
- Parenting
- Medical
- Allied Health Services
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Review
  url: https://doi.org/10.1007/BF03392760
  icon_pack: fas
  icon: pen-fancy

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://products.brookespublishing.com/Meaningful-Differences-in-the-Everyday-Experience-of-Young-American-Children-P14.aspx
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
