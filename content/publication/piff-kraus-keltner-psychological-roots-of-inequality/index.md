---
title: "Psychological Roots of Inequality: How Hierarchial Processes Produce and Perpetuate the Class Divide"
date: 2018-01-01
publishDate: 2019-12-04T05:08:32.816237Z
authors: ["Paul K. Piff", "Michael W. Kraus", "Dacher Keltner"]
tags:
- Class
publication_types: ["6"]
abstract: "Why does economic inequality continue to rise despite being disfavored and harmful to individuals and society? To better understand this inequality paradox, we advance an inequality maintenance model of social class. We detail a set of five propositions to encapsulate the psychological processes that perpetuate class division in society—disparities between the rich and the poor—and we review recent supporting data. With respect to the structural processes that define social class, we show that classdifferentiated experiences of threat, scarcity, and access to valued networks enhance economic inequality by compounding (dis)advantage in education, work, and relationships. With respect to social perceptual processes, we outline how social class is signaled and perceived during social interactions, triggering class-based stereotypes and patterns of distancing that reinforce inequality. With respect to ideological processes, we discuss how ideologies of merit legitimize economic inequality and bolster class division. With respect to moral–relational processes, we examine how class-based patterns of compassion, helping, and power seeking exacerbate economic inequality by concentrating resources among the upper class and constraining advancement among the lower class. Finally, with respect to intergroup processes, we posit that social class group identities catalyze difficulties in cross-class affiliation, asymmetric resource sharing, and class conflict, strengthening class division in society. We conclude with a discussion of new research and future directions that can address class disparities and, ultimately, help foster a more equal society."
featured: false
url_source: https://www.elsevier.com/books/advances-in-experimental-social-psychology/olson/978-0-12-814689-7
url_pdf: https://static1.squarespace.com/static/5432c0d8e4b0fc3eccdb0500/t/5a620a308165f505d9a19562/1516374577854/AESP.FINAL.pdf
doi: "10.1016/bs.aesp.2017.10.002"

links:
- name: Media
  url: post/kraus-the-roots-of-economic-inequality
  icon_pack: fas
  icon: pen
---

