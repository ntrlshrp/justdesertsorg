---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Brute Luck, Option Luck, and Equality of Initial Opportunities"
authors: 
- Peter Vallentyne
date: 2002-01-01
doi: "10.1086/339275"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-10T01:03:40-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Ethics"
publication_short: ""

abstract: "In the old days, material egalitarians tended to favor equality of outcome advantage, on some suitable conception of advantage (happiness, resources, etc.). Under the influence of Dworkin’s seminal articles on equality, contemporary material egalitarians have tended to favor equality of brute luck advantage---on the grounds that this permits people to be held appropriately accountable for the benefits and burdens of their choices. I shall argue, however, that a plausible conception of egalitarian justice requires neither that brute luck advantage always be equalized nor that people always bear the full cost of their voluntary choices. Instead, justice requires that initial opportunities for advantage be equalized---roughly along the lines suggested by Arneson and Cohen. Brute luck egalitarianism and initial opportunity egalitarianism are fairly similar in motivation, and as a result they have not been adequately distinguished. Once the two views are more clearly contrasted, equality of opportunity for advantage will, I claim, be seen to be a more plausible conception of equality."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://klinechair.missouri.edu/docs/brute_luck_option_luck_and_equality_of_initial_opportunities.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
