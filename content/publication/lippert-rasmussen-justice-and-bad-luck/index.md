---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Justice and Bad Luck"
authors: 
- Kasper Lippert-Rasmussen 
date: 2018-03-28
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T17:06:03-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "The Stanford Encyclopedia of Philosophy"
publication_short: ""

abstract: "Some people end up worse off than others partly because of their bad luck. For instance, some die young due to a genetic disease, whereas others live long lives. Are such differential luck induced inequalities unjust? Many are inclined to answer this question affirmatively. To understand this inclination, we need a clear account of what luck involves."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Luck
categories: 
- Philosophy
- Encyclopedia Article
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project: https://plato.stanford.edu/index.html
url_slides:
url_source: "https://plato.stanford.edu/entries/justice-bad-luck/"
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

# Justice and Bad Luck

Some people end up worse off than others partly because of their bad luck. For instance, some die young due to a genetic disease, whereas others live long lives. Are such differential luck induced inequalities unjust? Many are inclined to answer this question affirmatively. To understand this inclination, we need a clear account of what luck involves. On some accounts, luck nullifies responsibility. On others, it nullifies desert. It is often said that justice requires luck to be ‘neutralized’. However, it is contested whether a distributive pattern that eliminates the influence of luck can be described. Thus an agent’s level of effort—something few would initially see as a matter of luck—might be inseparable from her level of talent—something most would initially see as a matter of luck—and this might challenge standard accounts of just deviation from equality (or, for that matter, other favored distributive patterns). Critically, relational egalitarians argue that so-called luck egalitarians’ preoccupation with eliminating inequalities reflecting differential bad luck misconstrues justice, which, according to the former, is a matter of social relations having a suitably egalitarian character.

##### Table of Contents

1. Different Kinds of Luck
2. Distributive Justice
3. Thin Luck
4. Thick Luck
5. Independent Notions of Luck
6. How Much Luck is There?
7. Option Luck Versus Brute Luck
8. Neutralizing Luck and Equality
9. Non-Separability of Luck and Effort
10. Relational Egalitarianism and the Critique of Luck-Egalitarianism
- Bibliography
- Academic Tools
- Other Internet Resources
- Related Entries

[Continue Reading](https://plato.stanford.edu/entries/justice-bad-luck/)
