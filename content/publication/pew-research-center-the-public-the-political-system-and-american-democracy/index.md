---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The Public, the Political System and American Democracy"
authors: 
- Pew Research Center
date: 2018-04-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T19:01:39-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "At a time of growing stress on democracy around the world, Americans generally agree on democratic ideals and values that are important for the United States. But for the most part, they see the country falling well short in living up to these ideals, according to a new study of opinion on the strengths and weaknesses of key aspects of American democracy and the political system."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Survey
- Values
- Democracy
- Equality of Opportunity
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.people-press.org/wp-content/uploads/sites/4/2018/04/4-26-2018-Democracy-release-1.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.people-press.org/2018/04/26/the-public-the-political-system-and-american-democracy/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
> Some of the most pronounced partisan differences are in views of equal opportunity in the U.S. and whether the rights and freedoms of all people are respected. Republicans are twice as likely as Democrats to say “everyone has an equal opportunity to succeed” describes the United States very or somewhat well (74% vs. 37%). [p. 8]

> About eight-in-ten or more say it is very important for the country that the rights and freedoms of all are respected (84%), officials face serious consequences for misconduct (83%), that judges are not influenced by political parties (82%), and that everyone has an equal opportunity to succeed (82%). [pp. 23-24]

> More than half (55%) say the executive, legislative and judicial branches of government keep the others from having too much power; and 52% think the country is described well by the phrase “everyone has an equal opportunity to succeed.”" [p. 25]

[Continue Reading](https://www.people-press.org/wp-content/uploads/sites/4/2018/04/4-26-2018-Democracy-release-1.pdf)
