---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Emerging and Developing Economies Much More Optimistic than Rich Countries about the Future"
authors: 
- Pew Research Center
date: 2014-10-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-04T22:31:57-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "This report examines public opinion about opportunity and inequality around the world, including financial prospects for the next generation, the biggest factors to getting ahead in life and the causes of inequality. It is based on 48,643 interviews in 44 countries with adults 18 and older, conducted from March 17 to June 5, 2014. For more details, see survey methods and topline results. "

# Summary. An optional shortened abstract.
summary: ""

tags: 
- International Comparison
- Values
- Survey
- Economic Inequality
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.pewresearch.org/wp-content/uploads/sites/2/2014/10/Pew-Research-Center-Inequality-Report-FINAL-October-17-2014.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://www.pewresearch.org/global/2014/10/09/emerging-and-developing-economies-much-more-optimistic-than-rich-countries-about-the-future/
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

> Despite the long-term optimism that exists in many countries, there are widespread concerns about inequality. Majorities in all of the 44 nations polled say the gap between rich and poor is a big problem facing their countries, and majorities in 28 nations identify this as a very big problem. [p. 3]

> People blame inequality on a variety of causes, but they see their government’s economic policies as the top culprit. A global median of 29% say those policies are most to blame for the gap between rich and poor. Fewer people blame the amount of workers’ wages, the educational system, the fact that some work harder than others, trade, or the tax system. [p. 3]

> While inequality is considered a major challenge by a median of 60% across the 44 nations polled, higher numbers say rising prices and a lack of job opportunities (medians of 77%) are very big problems. [p. 4]

[Continue Reading](https://www.pewresearch.org/wp-content/uploads/sites/2/2014/10/Pew-Research-Center-Inequality-Report-FINAL-October-17-2014.pdf)
