---
title: "Equality of opportunity is linked to lower mortality in Europe"
date: 2019-01-01
publishDate: 2019-01-01
authors: ["Alexi Gugushvili", "Caspar Kaiser"]
publication_types: ["2"]
abstract: "Background This study investigates if intergenerational equality of opportunity is linked to mortality in 30 European countries. Equality of opportunity may lead to greater returns on health investments and, consequently, improved health outcomes. In turn, a perceived lack of fairness in the distribution of life chances and limited possibilities for upward intergenerational mobility can cause anxiety among individuals and gradually compromise their health. Methods We used information on 163 467 individuals’ and their parents’ Socio-Economic Index of Occupational Status from a large survey data set—the European Social Survey—to generate three complementary measures of equality of opportunity. We then linked these to administrative data on total, gender-specific and cause-specific mortality rates assembled by Eurostat from the national statistical offices. Results We found that lower equality of opportunity, measured by the attainment of individuals from the lowest and highest quartiles of socioeconomic status and by the overall intergenerational correlation in socioeconomic status, was related to higher mortality rates, particularly in relation to diseases of the nervous system and the sense organs, diseases of the respiratory system and external causes of mortality. Our measures of equality of opportunity were more consistently linked with mortality of men than women. Conclusion Equality of opportunity may be an important explanation of mortality that warrants further research. Measures that aim at facilitating intergenerational social mobility can be justified not only via normative considerations of equality of opportunity but also in terms of individuals’ chances to enjoy healthy lives."
featured: false
publication: "*Journal of Epidemiology and Community Health*"
tags: ["Equality", "Health Inequalities", "Mortality", "Social Epidemiology", "Social Inequalities", "Socio-Economic"]
url_source: "https://jech.bmj.com/content/early/2019/11/04/jech-2019-212540"
url_pdf: https://jech.bmj.com/content/jech/early/2019/11/04/jech-2019-212540.full.pdf
doi: "10.1136/jech-2019-212540"
---

