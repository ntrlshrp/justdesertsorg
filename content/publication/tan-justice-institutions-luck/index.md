---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Justice, Institutions, and Luck: The Site, Ground, and Scope of Equality"
authors: 
- Kok-Chor Tan
date: 2012-01-01
doi: "10.1093/acprof:oso/9780199588855.001.0001"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-03T20:55:29-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Kok-Chor Tan addresses three key questions in egalitarian distributive justice: Where does distributive equality matter?; Why does it matter?; And among whom does it matter? He argues for an institutional site for egalitarian justice, and suggests that the mitigation of arbitrariness or luck is the basis for distributive commitments. He also argues that distributive obligations are global in scope, applying between individuals across borders. Tan's objectives are tripartite: to clarify the basis of an institutional approach to justice; to establish luck egalitarianism as an account of the ground of equality; and to realize the global nature of egalitarian justice. The outcome is 'institutional luck egalitarianism'—a new cosmopolitan position on distributive justice."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
- name: Review
  url: https://doi.org/10.1017/S0953820812000520
  icon_pack: fas
  icon: pen-fancy
- name: Review
  url: https://doi.org/10.1086/673426
  icon_pack: fas
  icon: pen-fancy
- name: Review
  url: https://ndpr.nd.edu/news/justice-institutions-and-luck-the-site-ground-and-scope-of-equality/
  icon_pack: fas
  icon: pen-fancy

url_pdf: http://www.oxfordscholarship.com/view/10.1093/acprof:oso/9780199588855.001.0001/acprof-9780199588855
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
