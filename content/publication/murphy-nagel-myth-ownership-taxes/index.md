---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The Myth of Ownership: Taxes and Justice"
authors: 
- Liam Murphy
- Thomas Nagel
date: 2004-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-07-03T20:55:07-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "In a capitalist economy, taxes are the most important instrument by which the political system puts into practice a conception of economic and distributive justice. Taxes arouse strong passions, fueled not only by conflicts of economic self-interest, but by conflicting ideas of fairness. Taking as a guiding principle the conventional nature of private property, Murphy and Nagel show how taxes can only be evaluated as part of the overall system of property rights that they help to create. Justice or injustice in taxation, they argue, can only mean justice or injustice in the system of property rights and entitlements that result from a particular regime. Taking up ethical issues about individual liberty, interpersonal obligation, and both collective and personal responsibility, Murphy and Nagel force us to reconsider how our tax policy shapes our system of property rights."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: 
- Economic Justice
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
  - name: Review
    url: https://doi.org/10.1086/382169
    icon_pack: fas
    icon: pen-fancy

url_pdf: "https://global.oup.com/academic/product/the-myth-of-ownership-9780195176568?cc=us&lang=en&#"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
