---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Race and Economic Opportunity in the United States: An Intergenerational Perspective"
authors: 
- Raj Chetty
- Nathaniel Hendren
- Maggie Jones
- Sonya Porter
date: 2018-03-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-05T16:37:01-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "We study the sources of racial and ethnic disparities in income using de-identiﬁed longitudinal data covering nearly the entire U.S. population from 1989-2015. We document three sets of results. First, the intergenerational persistence of disparities varies substantially across racial groups. For example, Hispanic Americans are moving up signiﬁcantly in the income distribution across generations because they have relatively high rates of intergenerational income mobility. In contrast, black Americans have substantially lower rates of upward mobility and higher rates of downward mobility than whites, leading to large income disparities that persist across generations. Conditional on parent income, the black-white income gap is driven entirely by large diﬀerences in wages and employment rates between black and white men; there are no such diﬀerences between black and white women. Second, diﬀerences in family characteristics such as parental marital status, education, and wealth explain very little of the black-white income gap conditional on parent income. Diﬀerences in ability also do not explain the patterns of intergenerational mobility we document. Third, the black-white gap persists even among boys who grow up in the same neighborhood. Controlling for parental income, black boys have lower incomes in adulthood than white boys in 99% of Census tracts. Both black and white boys have better outcomes in low-poverty areas, but black-white gaps are larger on average for boys who grow up in such neighborhoods. The few areas in which black-white gaps are relatively small tend to be low-poverty neighborhoods with low levels of racial bias among whites and high rates of father presence among blacks. Black males who move to such neighborhoods earlier in childhood earn more and are less likely to be incarcerated. However, fewer than 5% of black children grow up in such environments. These ﬁndings suggest that reducing the black-white income gap will require eﬀorts whose impacts cross neighborhood and class lines and increase upward mobility speciﬁcally for black men."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
- Economic Inequality
- Race
- United States
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Media
  url: post/smith-america-land-of-equal-opportunity-still-not-there/
  icon_pack: fas
  icon: pen

url_pdf: http://www.equality-of-opportunity.org/assets/documents/race_paper.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: 
- equality-of-opportunity
- opportunity-insights

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
