---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Childhood Environment and Gender Gaps in Adulthood"
authors: 
- Raj Chetty
- Nathaniel Hendren
- Frina Lin
- Jeremy Majerovitz
- Benjamin Scuderi
date: 2016-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-11T18:13:36-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "We show that differences in childhood environments play an important role in shaping gender gaps in adulthood by documenting three facts using population tax records for children born in the 1980s. First, gender gaps in employment rates, earnings, and college attendance vary substantially across the parental income distribution. Notably, the traditional gender gap in employment rates is reversed for children growing up in poor families: boys in families in the bottom quintile of the income distribution are less likely to work than girls. Second, these gender gaps vary substantially across counties and commuting zones in which children grow up. The degree of variation in outcomes across places is largest for boys growing up in poor, single-parent families. Third, the spatial variation in gender gaps is highly correlated with proxies for neighborhood disadvantage. Low-income boys who grow up in high-poverty, high-minority areas work significantly less than girls. These areas also have higher rates of crime, suggesting that boys growing up in concentrated poverty substitute from formal employment to crime. Together, these findings demonstrate that gender gaps in adulthood have roots in childhood, perhaps because childhood disadvantage is especially harmful for boys."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Gender
- Equality of Opportunity
- Neighborhoods
- United States
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Media
  url: post/casselman-flowers-rich-kids-stay-rich-poor-kids-stay-poor/
  icon_pack: fas
  icon: pen

url_pdf: http://www.equality-of-opportunity.org/images/gender_paper.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: 
- opportunity-insights

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
