---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Responsibility and Distributive Justice"
authors: 
- Carl Knight
- Zofia Stemplowska
date: 2011-01-01
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-08-10T01:52:58-04:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Under what conditions are people responsible for their choices and the outcomes of those choices? How could such conditions be fostered by liberal societies? Should what people are due as a matter of justice depend on what they are responsible for? For example, how far should healthcare provision depend on patients' past choices? What values would be realized and which hampered by making justice sensitive to responsibility? Would it give people what they deserve? Would it advance or hinder equality? The explosion of philosophical interest in such questions has been fuelled by increased focus on individual responsibility in political debates. Political philosophers, especially egalitarians, have responded to such developments by attempting to map out the proper place for responsibility in theories of justice. Responsibility and Distributive Justice both reflects on these recent developments in normative political theory and moves the debate forwards. Written by established experts in the field and emerging scholars, it contains essays previously unpublished in academic books or journals. It will be of interest to researchers and students in political and moral philosophy."

# Summary. An optional shortened abstract.
summary: ""

tags: 
- Equality of Opportunity
- Responsibility
- Distributive Justice
categories: 
- Philosophy
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter
links:
- name: Review
  url: http://ndpr.nd.edu/news/26964-responsibility-and-distributive-justice/
  icon_pack: fas
  icon: pen-fancy
- name: Review
  url: https://journals.uvic.ca/index.php/pir/article/view/10887
  icon_pack: fas
  icon: pen-fancy

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: https://global.oup.com/academic/product/responsibility-and-distributive-justice-9780199565801?cc=us&lang=en&#
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
