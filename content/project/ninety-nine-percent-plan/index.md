---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The 99 Percent Plan"
summary: "The 99 Percent Plan is a joint Roosevelt Institute-Salon series that explores how progressives can shape a new vision for the economy."
authors: []
tags: 
- Economic Justice
categories: []
date: 1786-09-14

# Optional external URL for project (replaces project detail page).
external_link: "http://www.salon.com/topic/the_99_percent_plan/"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "Image credit: [Unknown](https://mediaproxy.salon.com/width/380/https://media.salon.com/2012/02/jefferson_lincoln_99-580x386.jpg)"
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
