---
title: Equality of Opportunity
summary: Policy oriented academic examination of opportunity in America.
tags:
- Equality of Opportunity
date: "1795-05-09"

# Optional external URL for project (replaces project detail page).
external_link: http://www.equality-of-opportunity.org/data/

image:
  caption: 
  focal_point: Smart
---
