---
title: Just Deserts
summary: Economic justice is giving to each exactly what they deserve.
tags:
- Economic Justice
- Equality of Opportunity
- justdeserts.org
date: "1790-09-19"

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

links:
# - icon: twitter
#   icon_pack: fab
#   name: Follow
#   url: https://twitter.com/georgecushen
url_code: "https://gitlab.com/ntrlshrp/desserts"
url_pdf: ""
url_poster: "img/ProductFlyer_9783030211257.pdf"
url_slides: ""
url_source: "https://www.springer.com/us/book/9783030211257"
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

> Economic justice is giving to each exactly what they deserve.

The principle of desert is the most intuitive principle of distribution for the vast majority of people.^[[Mulligan (2018), Ch. 3](/publication/mulligan-justice-and-the-meritocratic-state)]
Thus, it is critical that we know exactly what each of us deserves.

The answer is that we each deserve a _responsibility-sensitive_ allocation of benefits and burdens where inequalities between individuals are unjustified when dependent upon factors for which individuals are **not** responsible.

This project aims to identify those factors, describe the responsibility-sensitive distribution, and advocate for its realization through public policy.
