---
title: McCoy Family Center for Ethics in Society
summary: The McCoy Family Center for Ethics in Society is committed to bringing ethical reflection to bear on important social problems through research, teaching, and community engagement.
tags:
- Equality of Opportunity
date: "1787-03-03"

# Optional external URL for project (replaces project detail page).
external_link: https://ethicsinsociety.stanford.edu/

image:
  caption: 
  focal_point: Smart
---
