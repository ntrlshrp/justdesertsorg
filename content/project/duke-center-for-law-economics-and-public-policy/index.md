---
title: Duke Center for Law, Economics, and Public Policy
summary: Organizes scholarly activities in the area of law and economics, with a particular focus on the intersection between welfare economics and normative questions regarding legal frameworks, institutions and doctrines.
tags:
- Equality of Opportunity
date: "1796-09-11"

# Optional external URL for project (replaces project detail page).
external_link: https://law.duke.edu/laweconomicsandpublicpolicy/

image:
  caption: 
  focal_point: Smart
---
