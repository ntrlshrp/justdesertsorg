---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Political Philosophy for the Real World"
summary: "Political Philosophy for the Real World offers a home for original scholarly research that confronts the very problems ideal theory imagines away."
authors: []
tags: []
categories: []
date: 1784-03-12

# Optional external URL for project (replaces project detail page).
external_link: "https://www.routledge.com/Political-Philosophy-for-the-Real-World/book-series/PPRW"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Smart"
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

Political philosophers and applied ethicists often think in terms of ideal theory. In short, they ask what institutions, policies, or practices would work best if people had perfect motivations. While such work might help us imagine what utopia would look like, it offers little practical guidance.

Political Philosophy for the Real World offers a home for original scholarly research that confronts the very problems ideal theory imagines away, such as corruption, incentives, incompetence, rent-seeking, strategic free-riding and non-compliance, and political manipulation. The monographs and edited collections in this series integrate normative philosophy with the best empirical work in political science, economics, sociology, and psychology. By taking the incentives our institutions create and the motivations of individuals seriously, these books advocate for workable policy solutions that incorporate insights from the social sciences.
