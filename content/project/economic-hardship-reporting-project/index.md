---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Economic Hardship Reporting Project"
summary: "The aim of the Economic Hardship Reporting Project (EHRP) is to change the national conversation around poverty and economic insecurity.
The journalism we commission—from narrative features and photo essays to documentary films—puts a human face on financial instability."
authors: []
tags: []
categories: []
date: 1795-03-15

# Optional external URL for project (replaces project detail page).
external_link: "http://economichardship.org/"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
