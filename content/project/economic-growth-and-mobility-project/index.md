---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Economic Growth & Mobility Project"
summary: "The Economic Growth & Mobility Project is a new initiative of the Federal Reserve Bank of Philadelphia dedicated to promoting equal access to economic opportunity for all."
authors: []
tags: []
categories: []
date: 1795-03-15

# Optional external URL for project (replaces project detail page).
external_link: "https://www.philadelphiafed.org/egmp"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
