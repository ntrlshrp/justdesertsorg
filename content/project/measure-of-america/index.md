---
title: Measure of America
summary: Our mission is to provide easy-to-use yet methodologically sound tools for understanding well-being and opportunity in America and to stimulate fact-based dialogue about the issues we all care about.
tags:
- Equality of Opportunity
date: "1787-05-01"

# Optional external URL for project (replaces project detail page).
external_link: http://measureofamerica.org/

image:
  caption: 
  focal_point: Smart
---
