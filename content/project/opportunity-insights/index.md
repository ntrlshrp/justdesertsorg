---
title: Opportunity Insights
summary: Previously the Equality of Opportunity Project, Opportunity Insights uses big data to empower policymakers and civic leaders to create targeted local policy solutions that revive the American Dream.
tags:
- Equality of Opportunity
date: "1785-04-16"

# Optional external URL for project (replaces project detail page).
external_link: https://opportunityinsights.org/

image:
  caption: 
  focal_point: Smart
---
