---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Economic Security Project"
summary: "It’s time to end poverty and rebuild the middle class in America. We believe cash is an effective way to achieve that."
authors: []
tags: 
- Poverty
- Tax Policy
categories: []
date: 1795-03-15

# Optional external URL for project (replaces project detail page).
external_link: "https://economicsecurityproject.org/"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: "http://www.youtube.com/watch?v=uxYVQQlWMbs"

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
