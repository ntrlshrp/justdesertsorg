---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Center for Social Development: Child Development Accounts"
summary: "The Center for Social Development is a hub for implementing and testing applied social innovations that broaden well-being for individuals, families and communities."
authors: []
tags: 
- Equality of Opportunity
- Childhood Asset Transfers
categories: []
date: 1797-08-09

# Optional external URL for project (replaces project detail page).
external_link: "https://csd.wustl.edu/child-development-accounts/"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: "https://sites.wustl.edu/csd1/files/2018/04/CSD-REPORT.WebEdition.1.12.2016-1qop3n9.pdf"
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
