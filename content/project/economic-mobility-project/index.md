---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Economic Mobility Project"
summary: "Pew’s financial security and mobility project studies the financial well-being of American families and how their balance sheets relate to both short-term financial stability and longer-term economic mobility."
authors: []
tags: []
categories: []
date: 1795-03-15

# Optional external URL for project (replaces project detail page).
external_link: "https://www.pewtrusts.org/en/projects/archived-projects/economic-mobility-project"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "Image credit: [Unknown](https://www.pewtrusts.org/en/projects/archived-projects/economic-mobility-project)"
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: "https://www.pewtrusts.org/en/research-and-analysis/video/2011/economic-mobility-and-the-american-dream"

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
