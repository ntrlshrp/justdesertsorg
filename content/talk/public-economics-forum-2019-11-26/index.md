---
title: "Intergenerationally Disadvantaged: Newest Evidence and What it Means for Policy"
event: Public Economics Forum
event_url: https://melbourneinstitute.unimelb.edu.au/events-folder/conferences/pef-nov-19

location: National Portrait Gallery
address:
  street: King Edward Terrace, Parkes
  city: Canberra
  region: ACT
  postcode: '2600'
  country: Australia

summary: This Public Economic Forum brings together leading Australian and international thought leaders to discuss the latest research findings and innovative initiatives, and to provide evidence-based social policy recommendations.
abstract: "Early disadvantage exposes many to a lifelong cycle of deprivation that persists across generations. Evidence suggests that children exposed to adverse economic conditions and parental joblessness in their pre-adult life are likely to experience poor educational achievements and labour market outcomes that, in turn, determine future economic achievements and health outcomes. Understanding the role of early disadvantage in the Australian context is crucial given the considerable share of children exposed to income poverty and parental joblessness.

As such, this Public Economic Forum brings together leading Australian and international thought leaders to discuss the latest research findings and innovative initiatives, and to provide evidence-based social policy recommendations. Join us at the Public Economics Forum to hear about:

- The nature and persistence of intergenerational disadvantage in Australia and internationally;
- Examples of successful initiatives to reduce poverty and promote wellbeing;
- The roles of parenting and financial capability in developing successful outcomes;
- What this evidence means for the design of (new) policies and practices that will benefit disadvantaged families and Australian society."
draft: false

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2019-11-26T11:30:00"
date_end: "2019-11-26T14:30:00"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Image credit: [Public Economics Forum](https://melbourneinstitute.unimelb.edu.au/__data/assets/image/0007/3201937/PEF-0419-web.jpg)'
  focal_point: ""

# links:
# - icon: twitter
#   icon_pack: fab
#   name: Follow
#   url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

# Enable math on this page?
math: true
---

