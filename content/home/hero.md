+++
# Hero widget.
widget = "hero"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "Just Deserts"

# Hero image (optional). Enter filename of an image in the `static/img/` folder.
hero_media = "9783030211257.png"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  gradient_start = "#4caf50"
  gradient_end = "#2b94c3"
  
  # Background image.
  # image = ""  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `[cta]`.
#   Remove a link/note by deleting a cta/note block.
[cta]
  url = "https://www.springer.com/us/book/9783030211257"
  label = "Read the book"
  icon_pack = "fas"
  icon = "download"
  
[cta_alt]
  # url = "https://sourcethemes.com/academic/"
  # label = "View Documentation"
  url = "publication/dwyer-chance-merit-economic-inequality"
  label = "JustDeserts.org is also a companion to Chance, Merit, and Economic Inequality"

# Note. An optional note to show underneath the links.
[cta_note]
  # label = '<a class="js-github-release" href="https://sourcethemes.com/academic/updates" data-repo="gcushen/hugo-academic">Latest release<!-- V --></a>'
+++

**Economic justice is giving to each exactly what they deserve.**

An online portal to resources and writings on economic justice theories, policies, and analyses where the specific focus is the principle of desert, equality of opportunity, or luck.
